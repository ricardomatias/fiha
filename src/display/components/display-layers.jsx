import React from 'react';
import PropTypes from 'prop-types';
import ThreeDisplayLayer from './three-display-layer';
import CanvasDisplayLayer from './canvas-display-layer';
import PaperDisplayLayer from './paper-display-layer';
import './display-layers.scss';

import layersReducer from '../../controller/reducers/layers';

const {
  requestAnimationFrame,
  BroadcastChannel,
} = window;

const types = {
  three: ThreeDisplayLayer,
  canvas: CanvasDisplayLayer,
  paper: PaperDisplayLayer,
};

export function DisplayLayer(props) {
  const { id, type } = props;

  const LayerComponent = types[type];
  const className = `display-layer display-layer--${type}`;

  if (LayerComponent) return <LayerComponent {...props} className={className} />;

  return (
    <div className={className}>
      type {type} for {id} is unknown
    </div>
  );
}

DisplayLayer.propTypes = {
  id: PropTypes.string.isRequired,
  register: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  onExecutionError: PropTypes.func.isRequired,
};

const withKey = (Comp, register, onExecutionError, read, scale) => (props) => {
  const updatedProps = {
    ...props,
    animate: true,
    register,
    onExecutionError,
    read,
    scale,
  };
  // eslint-disable-next-line react/prop-types
  return <Comp key={props.id} {...updatedProps} />;
};

export default class DisplayLayers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scale: 1,
      layers: [],
    };

    this.data = {};
    this.read = this.read.bind(this);
    this.onExecutionError = this.onExecutionError.bind(this);

    this.register = this.register.bind(this);
    this.instances = {};
    this.animate = this.animate.bind(this);
    this.animate();

    this.broadcastChannelListener = this.broadcastChannelListener.bind(this);
    this.eachInstances = this.eachInstances.bind(this);
    this.updateLayers = this.updateLayers.bind(this);
  }

  componentDidMount() {
    if (this.broadcastChannel) return;
    this.broadcastChannel = new BroadcastChannel(this.props.broadcastChannelId);
    this.broadcastChannel.addEventListener('message', this.broadcastChannelListener, {
      passive: true,
      capture: false,
    });
    this.broadcastChannel.postMessage({
      type: 'REGISTER_DISPLAY',
      payload: {
        id: this.props.id,
      },
    });
  }

  componentWillUnmount() {
    if (!this.broadcastChannel) return;
    this.broadcastChannel.close();
    this.broadcastChannel = null;
  }

  get activeLayers() {
    return (this.state.layers || [])
      .filter(layer => layer && layer.active !== false);
  }

  register(id, instance) {
    this.instances[id] = instance;
  }

  eachInstances(func) {
    let instance;
    const keys = Object.keys(this.instances);
    for (let k = 0; k < keys.length; k += 1) {
      instance = this.instances[keys[k]];
      if (instance) func(instance, keys[k]);
    }
  }

  animate() {
    this.eachInstances(instance => instance.execAnimation());
    requestAnimationFrame(this.animate);
  }

  updateLayers(data) {
    const prevLayers = this.state.layers;
    const newLayers = layersReducer(prevLayers, data);
    const sameLayers = prevLayers === newLayers;
    if (!sameLayers) {
      this.setState({
        layers: newLayers,
      });
    }
  }

  // eslint-disable-next-line react/sort-comp
  onExecutionError(layerRole, layerId) {
    const { id } = this.props;
    return (err, lineCorrection = 0) => this.broadcastChannel.postMessage({
      type: 'EXECUTION_ERROR',
      payload: {
        display: id,
        layer: layerId,
        role: layerRole,
        stack: err.stack,
        lineCorrection,
      },
    });
  }

  broadcastChannelListener(evt) {
    const { type, payload } = evt.data;

    switch (type) {
      case 'UPDATE_DATA':
        this.data = payload;
        break;

      case 'DISPLAY_SCALE':
        this.setState({ scale: payload.scale || 1 });
        break;

      case 'REPLACE_LAYERS':
      case 'SORT_LAYERS':
      case 'EDIT_LAYER':
      case 'ADD_LAYER':
      case 'REMOVE_LAYER':
        this.updateLayers(evt.data);
        break;

      default:
        // eslint-disable-next-line no-console
        console.log('default update layer for', type);
    }
  }

  read(id, defaultValue = 0) {
    const { data } = this;
    if (!data || typeof data[id] === 'undefined') return defaultValue;
    return data[id];
  }

  render() {
    const layerInstances = this.activeLayers
      .map(withKey(
        DisplayLayer,
        this.register,
        this.onExecutionError,
        this.read,
        this.state.scale // eslint-disable-line
      ));
    return (
      <div
        ref={(el) => { this.dom = el; }}
        className="display-layers"
      >
        {layerInstances}
      </div>
    );
  }
}

DisplayLayers.propTypes = {
  id: PropTypes.string.isRequired,
  broadcastChannelId: PropTypes.string.isRequired,
};
