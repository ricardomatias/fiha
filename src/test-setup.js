const text = [
  'xxxxxx xxxxxx xxxxx   xxxx  xxxxx   xxxx  xxxxx  xxxxxx xxxxx ',
  '    x  x      x    x x    x x    x x    x x    x x      x    x',
  '   x   x      x    x x    x x    x x    x x    x x      x    x',
  '  x    xxxxx  xxxxx  x    x xxxxx  xxxxxx xxxxx  xxxxx  xxxxx ',
  ' x     x      x   x  x    x x      x    x x      x      x   x ',
  'xxxxxx xxxxxx x    x  xxxx  x      x    x x      xxxxxx x    x',
];

const crateOrigins = text
  .map((line, l) => line.split('')
    .map((char, c) => char.trim() && [c, text.length - l, 0]))
  .reduce((a, b) => [...a, ...b])
  .filter(coords => coords);

export default {
  layers: [
    {
      type: 'three',
      id: 'three',
      setup: `camera.lookAt(0, 0, 0);
const texture = new THREE.TextureLoader().load( 'static/textures/crate.gif' );
const geometry = new THREE.BoxBufferGeometry( 1, 1, 1 );
const material = new THREE.MeshBasicMaterial( { map: texture } );

cache.shuffle = (array) => {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

const crates = new THREE.Group();
cache.crateOrigins = ${JSON.stringify(crateOrigins, null, 2)};

cache.crateOrigins = cache.shuffle(cache.crateOrigins);
cache.crates = cache.crateOrigins.map((coords, c) => {
  const mesh = new THREE.Mesh( geometry, material );
  mesh.name = 'crate' + c;
  mesh.position.set(...coords);
  crates.add(mesh);
  return mesh;
});

const bbox = new THREE.Box3().setFromObject(crates);
const center = [
  (Math.abs(bbox.max.x) - Math.abs(bbox.min.x)) * -0.5,
  (Math.abs(bbox.max.y) - Math.abs(bbox.min.y)) * -0.5,
  (Math.abs(bbox.max.z) - Math.abs(bbox.min.z)) * -0.5,
];
cache.center = center;
crates.position.set(...center);
scene.add(crates);


const homeGeometry = new THREE.SphereBufferGeometry(300, 60, 40);
// invert the geometry on the x-axis so that all of the faces point inward
homeGeometry.scale(-1, 1, 1);
const homeMaterial = new THREE.MeshBasicMaterial({
  map: new THREE.TextureLoader().load('static/textures/2294472375_24a3b8ef46_o.jpg')
});
const homeMesh = new THREE.Mesh(homeGeometry, homeMaterial);
homeMesh.name = 'home';
scene.add(homeMesh);
console.info(center);
/*
const gridHelper = new THREE.GridHelper(300, 30);
gridHelper.name = 'gridHelper';
scene.add(gridHelper);
*/
`,

      animation: `const { crates, crateOrigins } = cache;
const now = (performance.now() * 0.0001) % 360;
const rad = Math.PI * 2 * (now + read('nk2-knob1'));
const dist = (Math.cos(rad) * 80) + 120;

if (cache.midiControlled) {
  camera.position.x = (read('nk2-knob1') * 4) - (127 * 2);//camera.position.x = Math.sin(rad) * dist;
  camera.position.y = (read('nk2-knob2') * 4) - (127 * 2);//Math.cos(rad) * dist * 0.5;
  camera.position.z = (read('nk2-knob3') * 4) - (127 * 2);//Math.cos(rad) * dist;
}
else {
  camera.position.x = camera.position.x = Math.sin(rad) * dist;
  camera.position.y = Math.cos(rad) * dist * 0.5;
  camera.position.z = Math.cos(rad) * dist;
}
camera.lookAt(0, 0, 0);

crates.forEach((crate, c) => {
 const origin = cache.crateOrigins[c];
 crate.position.z = origin[2] + (0.5 * (read('audio-frq')[(c)] - 0));
});

renderer.render(scene, camera);`,
    },


    {
      id: 'canvas',
      type: 'canvas',
      setup: `cache.started = cache.started || performance.now();
cache.heapUsed = cache.heapUsed || performance.memory.usedJSHeapSize;
cache.metrics = cache.metrics || {
  fps: [],
  heap: [],
};

ctx.strokeStyle = '#fff';
ctx.fillStyle = 'rgba(255, 255, 255, 0.5)';
ctx.textAlign = 'center';
ctx.textBaseline = 'middle';
ctx.font = '30px Monaco, Menlo, "Ubuntu Mono", Consolas, source-code-pro, monospace';`,

      animation: `const { started, metrics, last = 0 } = cache;
const { width, height } = ctx.canvas;
ctx.clearRect(0, 0, width, height);
const now = performance.now();
const heapUsed = performance.memory.usedJSHeapSize;
utils.printLines(ctx, [
  width + 'x' + height + 'px',
  'nk2-slider1: ' + read('nk2-slider1', 90),
  'audio-frq[16]: ' + read('audio-frq')[16],
  'audio-vol[16]: ' + read('audio-vol')[16],
  // heapUsed,
  // heapUsed - cache.heapUsed,
  (1000 / (now - last)).toFixed(2) + 'fpx',
  ((now - cache.started) * 0.001).toFixed(2) + 's',
]);
cache.last = now;
cache.heapUsed = heapUsed;
`,
    },
  ],
};
