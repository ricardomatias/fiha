import setupReducer from './setup';
import layersReducer from './layers';

export default {
  setup: setupReducer,
  layers: layersReducer,
};
