import korgkp3 from './devices/korg/kp3';
import korgnk2 from './devices/korg/nk2';
import akailpd8 from './devices/akai/lpd8';
import novationlpm from './devices/novation/lpm';
import logger from '../../../logger';

const log = logger('MIDI', 'red');

const midiMappings = {
  'KORG INC.': {
    'KP3 MIDI 1': korgkp3,
    'nanoKONTROL2 MIDI 1': korgnk2,
  },
  'AKAI professional LLC': {
    'LPD8 MIDI 1': akailpd8,
  },
  'Focusrite A.E. Ltd': {
    'Launchpad Mini MIDI 1': novationlpm,
  },
};


export class MIDIMapping {
  constructor({
    device,
    mappings,
    dispatch,
  }) {
    this.device = device;
    this.mappings = mappings;
    this.dispatch = dispatch;
    log('mappings', Object.keys(mappings));
    this.onMidiMessage = this.onMidiMessage.bind(this);
    this.bindEvents();

    this.listener = (data) => {
      const id = `${data.prefix}-${data.name}`.replace(/[^a-z0-9]/ig, '-');
      this.dispatch({
        type: 'UPDATE_DATA',
        payload: {
          [id]: data.velocity,
        },
      });
    };
  }

  onMidiMessage(evt) {
    const info = this.mappings(evt.data);
    if (info.type === 248) {
      return;
    }
    // log('midimessage', this.device.name, info);
    this.listener(info);
  }

  bindEvents() {
    this.device.addEventListener('midimessage', this.onMidiMessage);
  }

  unbindEvents() {
    this.device.removeEventListener('midimessage', this.onMidiMessage);
  }
}

export class FihaMIDI {
  constructor({ dispatch }) {
    this.inputs = [];
    this.midiMappings = midiMappings || {};
    this.dispatch = dispatch;

    this.onStateChange = this.onStateChange.bind(this);
    this.addInput = this.addInput.bind(this);

    log('capable?', FihaMIDI.capable);
    if (FihaMIDI.capable) {
      // eslint-disable-next-line no-console
      this.request().catch(err => console.warn(err.stack));
    }
  }

  static get capable() {
    return !!navigator.requestMIDIAccess;
  }

  getMappings(manufacturer, name) {
    const m = this.midiMappings;
    if (!m[manufacturer] || !m[manufacturer][name]) {
      return null;
    }
    return m[manufacturer][name];
  }

  addInput(input) {
    const mappings = this.getMappings(input.manufacturer, input.name);
    if (!mappings) {
      if (input.name !== 'Midi Through Port-0') {
        // eslint-disable-next-line
        console.warn('MIDI: Unrecognized controller %s from %s', input.name, input.manufacturer);
      }
    } else {
      log('addInput', input.name);
      this.inputs.push(new MIDIMapping({
        mappings,
        device: input,
        dispatch: this.dispatch,
      }));
    }
  }

  onStateChange() {
    const { MIDIAccess } = this;

    log('statechange', MIDIAccess.inputs.size);

    this.inputs.forEach(input => input.unbindEvents());

    this.inputs = [];
    MIDIAccess.inputs.forEach(this.addInput);
  }

  close() {
    this.inputs.forEach(input => input.unbindEvents());
  }

  async request() {
    const MIDIAccess = await navigator.requestMIDIAccess();
    if (this.MIDIAccess !== MIDIAccess) {
      log('MIDIAccess', MIDIAccess);
      MIDIAccess.addEventListener('statechange', this.onStateChange);
      this.MIDIAccess = MIDIAccess;
    }
    this.onStateChange();
  }
}

let singleton;
export default function (dispatch) {
  // singleton = singleton || new FihaMIDI(dispatch);
  singleton = new FihaMIDI(dispatch);
  return singleton;
}
