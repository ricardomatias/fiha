/* eslint-disable */
import actionsThrottler from '../../src/controller/reducers/action-throttler';

const mockFn = jest.fn(() => {});

describe.skip('action throttle', () => {
  it('reduces the amount of actions which have the same type and id', () => {
    const actionsIn = [
      {
        type: 'ACTION_TYPE_A',
        id: 'a',
        value: 'a',
      },
      {
        type: 'ACTION_TYPE_A',
        id: 'a',
        value: 'b',
      },
      {
        type: 'ACTION_TYPE_A',
        id: 'a',
        value: 'c',
      },
      {
        type: 'ACTION_TYPE_B',
        id: 'a',
        value: 'a',
      },
      {
        type: 'ACTION_TYPE_B',
        id: 'a',
        value: 'b',
      },
      {
        type: 'ACTION_TYPE_C',
        id: 'a',
        value: 'c',
      },
    ];

    const actionsOut = [
      {
        type: 'ACTION_TYPE_A',
        id: 'a',
        value: 'c',
      },
      {
        type: 'ACTION_TYPE_B',
        id: 'a',
        value: 'b',
      },
      {
        type: 'ACTION_TYPE_C',
        id: 'a',
        value: 'c',
      },
    ];

    expect(actionsThrottler()(actionsIn)).toEqual(actionsOut);
  });
});
