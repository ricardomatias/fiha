import MathTools from './math-tools';
import MiscTools from './misc-tools';

const fetchCache = {};

const {
  PI2,
  arrayMax,
  arrayMin,
  arrayAvg,
  sDiv,
} = MathTools;

const { noop } = MiscTools;

export default function canvasTools(ctx) {
  const { canvas } = ctx;

  const width = (div = 1) => canvas.width * (1 / div);
  width.category = 'canvas';
  width.type = 'function';
  width.description = 'Get width of the canvas or a division of it.';
  width.snippet = 'const val = width(10); // canvas width divided 10 times';

  const height = (div = 1) => canvas.height * (1 / div);
  height.category = 'canvas';
  height.type = 'function';
  height.description = 'Get height of the canvas or a division of it.';
  height.snippet = 'const val = height(10); // canvas height divided 10 times';

  const vw = (count = 1) => canvas.width * 0.01 * count;
  vw.category = 'canvas';
  vw.type = 'function';
  vw.description = 'Get width of the canvas or percents of it.';
  vw.snippet = 'const val = vw(10);';

  const vh = (count = 1) => canvas.height * 0.01 * count;
  vh.category = 'canvas';
  vh.type = 'function';
  vh.description = 'Get height of the canvas or percents of it.';
  vh.snippet = 'const val = vh(10);';

  const vmin = (count = 1) => (Math.min(canvas.width, canvas.height) * 0.01 * count);
  vmin.category = 'canvas';
  vmin.type = 'function';
  vmin.description = 'Get shortest side of the canvas or percents of it.';
  vmin.snippet = 'const val = vmin(10);';

  const vmax = (count = 1) => (Math.max(canvas.width, canvas.height) * 0.01 * count);
  vmax.category = 'canvas';
  vmax.type = 'function';
  vmax.description = 'Get longest side of the canvas or percents of it.';
  vmax.snippet = 'const val = vmax(10);';

  const textLines = (lines = [], opts = {}) => {
    const {
      x = width(2),
      y = height(2),
      position = 'center',
      fill = 'white',
      stroke = false,
      lineHeight = 1.618,
    } = opts;
    const lh = (parseInt(ctx.font, 10) || 20) * lineHeight;
    const linesHeight = lines.length * lh;
    let top = y - (linesHeight * 0.5);
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'center';

    switch (position) {
      case 'top':
        top = y;
        break;

      case 'bottom':
        top = y - linesHeight;
        break;

      case 'left':
        ctx.textAlign = 'left';
        break;

      case 'right':
        ctx.textAlign = 'right';
        break;

      case 'top-left':
        top = y;
        ctx.textAlign = 'left';
        break;

      case 'top-right':
        top = y;
        ctx.textAlign = 'right';
        break;

      case 'bottom-left':
        top = y - linesHeight;
        ctx.textAlign = 'left';
        break;

      case 'bottom-right':
        top = y - linesHeight;
        ctx.textAlign = 'right';
        break;

      default:
    }


    if (stroke && stroke !== true) ctx.strokeStyle = stroke;
    if (fill && fill !== true) ctx.fillStyle = fill;

    let line;
    let h;
    for (let l = 0; l < lines.length; l += 1) {
      line = lines[l];
      h = (lh * (l + 0.5));
      if (stroke) ctx.strokeText(line, x, top + h);
      if (fill) ctx.fillText(line, x, top + h);
    }
  };
  textLines.category = 'canvas';
  textLines.type = 'function';
  textLines.description = 'Draws text on different lines.';
  textLines.snippet = `textLines([
  'first line',
  'second lines',
],
// default values
{
  x: width(2),
  y: height(2),
  position: 'center',
  // can be a color or false
  fill: 'white',
  // can be a color or false
  stroke: false,
  lineHeight: 1.618,
});`;
  textLines.args = [
    {
      name: 'lines',
      type: 'array',
      optional: true,
    },
  ];

  const mediaType = url => (/\.(mp4|webm)$/.test(url) ? 'video' : 'image');
  mediaType.category = 'media';
  mediaType.type = 'function';
  mediaType.description = 'Attempts to quickly determine the type of an URL.';

  const tools = {
    ...MathTools,
    width,
    height,
    vw,
    vh,
    vmin,
    vmax,
    textLines,


    clear: () => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    },


    copy: (
      sx = 0,
      sy = 0,
      sw = canvas.width,
      sh = canvas.height,
      dx = 0,
      dy = 0,
      dw = canvas.width,
      dh = canvas.height,
    ) => {
      const el = document.createElement('canvas');
      el.width = canvas.width;
      el.height = canvas.height;
      el.getContext('2d')
        .drawImage(canvas, sx, sy, sw, sh, dx, dy, dw, dh);
      return el;
    },


    // paste: (
    //   el,
    //   sx = 0,
    //   sy = 0,
    //   sw = el.width || el.videoWidth,
    //   sh = el.height || el.videoHeight,
    //   dx = 0,
    //   dy = 0,
    //   dw = canvas.width,
    //   dh = canvas.height,
    // ) => {
    //   ctx.drawImage(el, sx, sy, sw, sh, dx, dy, dw, dh);
    // },


    pasteImage: (src, opts = {}) => {
      const w = src.width || src.videoWidth;
      const h = src.height || src.videoHeight;
      const wp = canvas.width / w;
      const hp = canvas.height / h;
      const p = Math.abs(wp) < Math.abs(hp) ? wp : hp;
      const ddw = p * w;
      const ddh = p * h;
      const {
        sx = 0,
        sy = 0,
        sw = w,
        sh = h,
        dx = (canvas.width - ddw) * 0.5,
        dy = (canvas.height - ddh) * 0.5,
        dw = ddw,
        dh = ddh,
      } = opts;
      ctx.drawImage(src, sx, sy, sw, sh, dx, dy, dw, dh);
    },


    pasteContain: (src, opts = {}) => {
      const w = src.width || src.videoWidth;
      const h = src.height || src.videoHeight;
      const wp = canvas.width / w;
      const hp = canvas.height / h;
      const p = Math.abs(wp) < Math.abs(hp) ? wp : hp;
      const ddw = p * w;
      const ddh = p * h;
      const {
        sx = 0,
        sy = 0,
        sw = w,
        sh = h,
        dx = (canvas.width - ddw) * 0.5,
        dy = (canvas.height - ddh) * 0.5,
        dw = ddw,
        dh = ddh,
      } = opts;
      ctx.drawImage(src, sx, sy, sw, sh, dx, dy, dw, dh);
    },


    pasteCover: (src, opts = {}) => {
      const w = src.width || src.videoWidth;
      const h = src.height || src.videoHeight;
      const wp = canvas.width / w;
      const hp = canvas.height / h;
      const p = Math.abs(wp) > Math.abs(hp) ? wp : hp;
      const ddw = p * w;
      const ddh = p * h;
      const {
        sx = 0,
        sy = 0,
        sw = w,
        sh = h,
        dx = (canvas.width - ddw) * 0.5,
        dy = (canvas.height - ddh) * 0.5,
        dw = ddw,
        dh = ddh,
      } = opts;
      ctx.drawImage(src, sx, sy, sw, sh, dx, dy, dw, dh);
    },


    fontSize: (size, unit = vmin) => {
      const parts = ctx.font.split(' ');
      if (parts.length === 2) {
        ctx.font = `normal ${unit(size)}px ${parts[1]}`;
      } else {
        ctx.font = `${parts[0]} ${unit(size)}px ${parts[2]}`;
      }
    },

    fontFamily: (val = 'sans-serif') => {
      const parts = ctx.font.split(' ');
      if (parts.length === 2) {
        ctx.font = `normal ${parts[0]} ${val}`;
      } else {
        ctx.font = `${parts[0]} ${parts[1]} ${val}`;
      }
    },

    fontWeight: (weight = 'normal') => {
      const parts = ctx.font.split(' ');
      if (parts.length === 2) {
        ctx.font = `${weight} ${parts[0]} ${parts[1]}`;
      } else {
        ctx.font = `${weight} ${parts[1]} ${parts[2]}`;
      }
    },

    // fontLoad: (val) => {
    // },


    fetch: (url) => {
      fetchCache[url] = fetchCache[url] || new Promise((resolve, reject) => {
        fetch(url, { mode: 'cors' })
          .then(response => response.blob())
          .then((blob) => {
            let obj;
            const type = mediaType(url);
            if (type === 'video') {
              obj = document.createElement('video');
              obj.src = URL.createObjectURL(blob);
              obj.muted = true;
              obj.loop = true;
              obj.play();
            } else {
              obj = new Image();
              obj.src = URL.createObjectURL(blob);
            }
            resolve(obj);
          })
          .catch(reject);
      });

      return fetchCache[url];
    },


    plot: ({
      data = [],
      min = arrayMin(data),
      max = arrayMax(data),
      samples = data.length,
      floor = 0,
      top = 0,
      bottom = canvas.height,
      left = 0,
      right = canvas.width,
      legend = 'top-left',
      color = null,
    }) => {
      const pWidth = right - left;
      const pHeight = bottom - top;
      const diff = Math.abs(min - max);
      const w = sDiv(pWidth, samples - 1);
      const h = sDiv(pHeight, diff);

      if (color) {
        ctx.strokeStyle = color;
      }

      ctx.setLineDash([3, 5]);
      ctx.beginPath();
      ctx.moveTo(left, bottom - (h * (floor - min)));
      ctx.lineTo(right, bottom - (h * (floor - min)));
      ctx.stroke();
      ctx.closePath();
      ctx.setLineDash([]);

      ctx.beginPath();

      let val;
      let py;
      let px;
      for (let v = 0; v < data.length; v += 1) {
        val = data[v];
        py = top + (pHeight - (h * (val - min)));
        px = left + (w * v);
        if (v) {
          ctx.lineTo(px, py);
        } else {
          ctx.moveTo(px, py);
        }
      }
      ctx.stroke();
      ctx.closePath();

      if (!legend) return;
      let ptop;
      let pleft;
      switch (legend) {
        case 'top':
          ptop = top;
          pleft = left + (pWidth * 0.5);
          break;

        case 'bottom':
          ptop = bottom;
          pleft = left + (pWidth * 0.5);
          break;

        case 'left':
          ptop = top + (pHeight * 0.5);
          pleft = left;
          break;

        case 'right':
          ptop = top + (pHeight * 0.5);
          pleft = right;
          break;

        case 'top-left':
          ptop = top;
          pleft = left;
          break;

        case 'top-right':
          ptop = top;
          pleft = right;
          break;

        case 'bottom-left':
          ptop = bottom;
          pleft = left;
          break;

        case 'bottom-right':
          ptop = bottom;
          pleft = right;
          break;

        default:
          ptop = top + (pHeight * 0.5);
          pleft = left + (pWidth * 0.5);
      }

      const originalFont = ctx.font;
      ctx.font = `${vmin(2)}px monospace`;
      textLines(
        [
          `min: ${min.toFixed(3)}`,
          `max: ${max.toFixed(3)}`,
          `avg: ${arrayAvg(data).toFixed(3)}`,
        ],
        {
          x: pleft,
          y: ptop,
          position: legend,
          fill: color || 'white',
          stroke: 'black',
        },
      );
      ctx.font = originalFont;
    },


    repeat: (times = 1, func = noop) => {
      for (let t = 0; t < times; t += 1) {
        func(t, times);
      }
    },


    circle: ({
      x,
      y,
      radius = 10,
      stroke = null,
      fill = null,
    }) => {
      if (stroke) ctx.strokeStyle = stroke;
      if (fill) ctx.fillStyle = fill;

      ctx.beginPath();
      ctx.arc(x, y, radius, 0, PI2);
      ctx.closePath();

      if (stroke) ctx.stroke();
      if (fill) ctx.fill();
    },


    polygon: ({
      x,
      y,
      sides = 3,
      radius = 10,
      tilt = 0,
      stroke = null,
      fill = null,
    }) => {
      let px;
      let py;
      const a = PI2 * (1 / sides);
      if (stroke) ctx.strokeStyle = stroke;
      if (fill) ctx.fillStyle = fill;

      ctx.beginPath();
      for (let s = 0; s < sides; s += 1) {
        px = x + (Math.sin(tilt + (a * s)) * radius);
        py = y + (Math.cos(tilt + (a * s)) * radius);
        ctx[!s ? 'moveTo' : 'lineTo'](px, py);
      }
      ctx.closePath();

      if (stroke) ctx.stroke();
      if (fill) ctx.fill();
    },


    grid: (rows = 4, cols = 4, func = noop) => {
      const xs = width(rows);
      const ys = height(cols);
      let x;
      let y;
      let n = 0;

      for (let r = 0; r < rows; r += 1) {
        for (let c = 0; c < cols; c += 1) {
          x = xs * (r + 0.5);
          y = ys * (c + 0.5);
          func(x, y, n, r, c);
          n += 1;
        }
      }
    },
  };


  tools.clear.category = 'canvas';
  tools.clear.type = 'function';
  tools.clear.description = 'Clears the whole canvas';
  tools.clear.snippet = 'clear();';

  tools.fontSize.category = 'canvas';
  tools.fontSize.type = 'function';
  tools.fontSize.description = 'Changes the size of text';
  // eslint-disable-next-line
  tools.fontSize.snippet = '// pass an amount and optionally a sizing function\nfontSize(2, vmax);';

  tools.fontWeight.category = 'canvas';
  tools.fontWeight.type = 'function';
  tools.fontWeight.description = `Changes the weight of the text font.
  The possible values depend on the font but common values could be:
  * 100, 200, 300... to 700
  * "lighter", "light", "normal", "bold" and "bolder"`;
  tools.fontWeight.snippet = 'fontWeight(700);';

  tools.fontFamily.category = 'canvas';
  tools.fontFamily.type = 'function';
  tools.fontFamily.description = `Common values are "sans", "sans-serif" or "monospace" but
  you could use other font families if they are loaded or installed locally.`;
  tools.fontFamily.snippet = 'fontFamily(700);';

  tools.plot.category = 'canvas';
  tools.plot.type = 'function';
  tools.plot.description = 'Plot given data';
  tools.plot.snippet = `const data = read('frequencies', []);
plot({
  data,
  min: arrayMin(data),
  max: arrayMax(data),
  samples: data.length,
  floor: arrayAvg(data),
  top: 0,
  bottom: height(),
  left: 0,
  right: width(),
  legend: 'top-left',
  color: 'lime',
});`;

  tools.repeat.category = 'canvas';
  tools.repeat.type = 'function';
  tools.repeat.description = 'Executes a callback a given amount of times';
  tools.repeat.snippet = 'repeat(3, (t, times) => console.info(t, times));';

  tools.circle.category = 'canvas';
  tools.circle.type = 'function';
  tools.circle.description = 'Draws a circle';
  tools.circle.snippet = `circle({
  x: width(3),
  y: height(2),
  radius: vmin(25),
  stroke: null,
  fill: '#fff',
});`;

  tools.polygon.category = 'canvas';
  tools.polygon.type = 'function';
  tools.polygon.description = 'Draws a polygon';
  tools.polygon.snippet = `polygon({
  x: width(3),
  y: height(2),
  sides: 5,
  radius: vmin(25),
  tilt: 6,
  stroke: null,
  fill: '#fff',
});`;

  tools.grid.category = 'canvas';
  tools.grid.type = 'function';
  tools.grid.description = 'Executes a callback with grid coordinates as arguments. The first and second arguments represent the number of rows and columns. The third argument is the callback to be executed.';
  tools.grid.snippet = `grid(3, 4, (x, y, n, r, c) => {
  // circle({ x, y, radius: vmin(5), fill: 'white' })
});`;


  tools.copy.category = 'canvas';
  tools.copy.type = 'function';
  tools.copy.description = 'Copy the canvas as an image for later manipulation';
  tools.copy.snippet = 'cache.img = copy();';


  tools.pasteImage.category = 'canvas';
  tools.pasteImage.type = 'function';
  tools.pasteImage.description = 'Put an image on the canvas';
  tools.pasteImage.snippet = `/*
You should fetch the image in the setup script and make it available in the cache

fetch('https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Golden_tabby_and_white_kitten_n03.jpg/1024px-Golden_tabby_and_white_kitten_n03.jpg')
  .then(img => cache.img = img)
  .catch(err => console.info(err));
*/

// in a animation script
clear();
if (!cache.img) return;
pasteImage(cache.img);
`;


  tools.pasteContain.category = 'canvas';
  tools.pasteContain.type = 'function';
  // eslint-disable-next-line
  tools.pasteContain.description = 'Put an image on the canvas so that the image is contained in the available surface';
  tools.pasteContain.snippet = `/*
You should fetch the image in the setup script and make it available in the cache

fetch('https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Golden_tabby_and_white_kitten_n03.jpg/1024px-Golden_tabby_and_white_kitten_n03.jpg')
  .then(img => cache.img = img)
  .catch(err => console.info(err));
*/

// in a animation script
clear();
if (!cache.img) return;
pasteContain(cache.img);
`;


  tools.pasteCover.category = 'canvas';
  tools.pasteCover.type = 'function';
  // eslint-disable-next-line
  tools.pasteCover.description = 'Put an image on the canvas so that the image covers the whole surface';
  tools.pasteCover.snippet = `/*
You should fetch the image in the setup script and make it available in the cache

fetch('https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Golden_tabby_and_white_kitten_n03.jpg/1024px-Golden_tabby_and_white_kitten_n03.jpg')
  .then(img => cache.img = img)
  .catch(err => console.info(err));
*/

// in a animation script
clear();
if (!cache.img) return;
pasteCover(cache.img);
`;


  tools.fetch.category = 'canvas';
  tools.fetch.type = 'function';
  // eslint-disable-next-line
  tools.fetch.description = 'Fetch a media (image or video) for later use';
  tools.fetch.snippet = `// You should fetch the image in the setup script
fetch('https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Golden_tabby_and_white_kitten_n03.jpg/1024px-Golden_tabby_and_white_kitten_n03.jpg')
  .then(img => cache.img = img)
  .catch(err => console.info(err));
`;


  const instKeys = Object.keys(ctx)
    .filter(key => !['function', 'object'].includes(key));
  const proto = ctx.constructor.prototype;

  Object.keys(proto)
    .filter(key => !instKeys.includes(key))
    .forEach((key) => {
      if (key === 'canvas') return;

      if (typeof ctx[key] === 'function') {
        tools[key] = ctx[key].bind(ctx);

        tools[key].category = 'canvas';
        tools[key].type = 'function';
        tools[key].description = `Utility function for CanvasRenderingContext2D.${key}()`;
      } else if (!tools[key]) {
        tools[key] = (value = ctx[key]) => {
          if (ctx[key] !== value) ctx[key] = value;
          return ctx[key];
        };

        tools[key].category = 'canvas';
        tools[key].type = 'function';
        tools[key].description = `Utility function for CanvasRenderingContext2D.${key} (read or write)`;
      }

      // eslint-disable-next-line
      tools[key].link = `https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/${key}`;
    });

  return tools;
}
