import React from 'react';
import PropTypes from 'prop-types';
import './audio-manager.scss';
import mathTools from '../../display/math-tools';

const { arrayAvg } = mathTools;
const pad = (n, z = 2) => `00${n}`.slice(-z);

const {
  addEventListener,
  cancelAnimationFrame,
  removeEventListener,
  requestAnimationFrame,
} = window;

export function AudioManagerInput({
  label,
  properties,
}) {
  const id = properties.name;
  return (
    <div className="form-group row mb-0">
      <label className="col-form-label col-form-label-sm col-3" htmlFor={id}>{label}</label>
      <div className="col-7">
        <input className="form-control form-control-sm" type="range" id={id} {...properties} />
      </div>
      <span className="col-form-legend col-form-legend- col-2">{properties.value}</span>
    </div>
  );
}

AudioManagerInput.propTypes = {
  label: PropTypes.string.isRequired,
  properties: PropTypes.object.isRequired,
};


export default class AudioManager extends React.Component {
  constructor(props) {
    super(props);

    const {
      minDecibels,
      maxDecibels,
      smoothingTimeConstant,
      fftSize,
    } = props.audio.settings;

    this.state = {
      minDecibels,
      maxDecibels,
      smoothingTimeConstant,
      fftSize,
    };

    this.mouseListener = this.mouseListener.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.handleBpmTab = this.handleBpmTab.bind(this);
    this.handleBpmInpChange = this.handleBpmInpChange.bind(this);
    this.drawScales = this.drawScales.bind(this);
    this.update = this.update.bind(this);
  }

  componentDidMount() {
    const opts = {
      passive: true,
      capture: false,
    };
    addEventListener('resize', this.handleResize, opts);
    if (this.canvas) this.canvas.addEventListener('mousemove', this.mouseListener, opts);
    this.handleResize();
    requestAnimationFrame(this.update);
  }

  componentWillUnmount() {
    const opts = {
      passive: true,
      capture: false,
    };
    removeEventListener('resize', this.handleResize, opts);
    if (this.canvas) this.canvas.removeEventListener('mousemove', this.mouseListener, opts);
    cancelAnimationFrame(this.aFRID);
  }

  mouseListener(evt) {
    const { canvas } = this;
    const x = evt.offsetX - (canvas.width / 2);
    const y = evt.offsetY - (((canvas.height - 60) / 2) + 15);
    this.centerDist = Math.sqrt((x * x) + (y * y));
    this.mouseX = evt.offsetX;
    this.mouseY = evt.offsetY;
  }

  handleResize() {
    if (!this.canvas) return;
    const { canvas } = this;
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    this.ctx = canvas.getContext('2d');
  }

  drawScales(bufferLength) {
    const { ctx, canvas } = this;
    const x = canvas.width * 0.5;
    const y = ((canvas.height - 60) * 0.5) + 15;
    const r = Math.min(x, y) - 30;
    const rad = Math.PI * 2;
    const sub = !(bufferLength % 16) ? 16 : 4;

    ctx.setLineDash([10, 10]);

    ctx.font = '10px monospace';
    ctx.globalAlpha = 0.5;
    // eslint-disable-next-line
    ctx.fillStyle = ctx.strokeStyle = '#fff';

    // eslint-disable-next-line
    let i, a, ax, ay, bx, by, lx, ly, ca, sa;
    for (i = 0; i < sub; i += 1) {
      a = ((rad / sub) * i) - Math.PI;
      ca = Math.cos(a);
      sa = Math.sin(a);
      ax = Math.round(x + (ca * (r / 10)));
      ay = Math.round(y + (sa * (r / 10)));
      bx = Math.round(x + (ca * (r - 5)));
      by = Math.round(y + (sa * (r - 5)));
      lx = Math.round(x + (ca * r));
      ly = Math.round(y + (sa * r));

      ctx.beginPath();
      ctx.moveTo(ax, ay);
      ctx.lineTo(bx, by);

      ctx.textAlign = 'center';
      if (lx < x) {
        ctx.textAlign = 'right';
      } else if (lx > x) {
        ctx.textAlign = 'left';
      }

      ctx.textBaseline = 'middle';
      if (ly < y) {
        ctx.textBaseline = 'bottom';
      } else if (ly > y) {
        ctx.textBaseline = 'top';
      }
      ctx.globalAlpha = 1;
      ctx.fillText((bufferLength / sub) * i, lx, ly);
      ctx.globalAlpha = 0.5;

      ctx.stroke();
      ctx.closePath();
    }

    const dist = r * 0.005;
    ctx.beginPath();
    ctx.arc(x, y, 64 * dist, 0, rad);
    ctx.closePath();
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(x, y, 128 * dist, 0, rad);
    ctx.closePath();
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(x, y, 192 * dist, 0, rad);
    ctx.closePath();
    ctx.stroke();

    ctx.setLineDash([]);

    ctx.globalAlpha = 1;
  }

  update() {
    const { ctx, canvas } = this;
    if (!canvas) {
      requestAnimationFrame(this.update);
      return;
    }

    const { audio } = this.props;
    const { width, height } = canvas;
    const { analyser, frequencies, volume } = audio;
    ctx.clearRect(0, 0, width, height);
    // eslint-disable-next-line
    ctx.fillStyle = ctx.strokeStyle = this.color;

    const bufferLength = analyser.frequencyBinCount;
    this.drawScales(bufferLength);

    const x = width * 0.5;
    const y = ((height - 60) * 0.5) + 15;
    const r = Math.min(x, y) - 30;
    const rad = Math.PI * 2;

    ctx.font = '13px monospace';
    ctx.textAlign = 'center';

    // eslint-disable-next-line
    let i, a, f, td, lx, ly, val, min = 0, max = 0, avg = 0;
    // eslint-disable-next-line
    ctx.strokeStyle = ctx.fillStyle = '#e6db74';
    ctx.beginPath();
    for (i = 0; i < bufferLength; i += 1) {
      val = frequencies[i];
      avg += val;
      min = Math.min(min, val);
      max = Math.max(max, val);

      a = ((rad / bufferLength) * i) - Math.PI;
      f = (r / 100) * (val * 0.5);
      lx = Math.round(x + (Math.cos(a) * f));
      ly = Math.round(y + (Math.sin(a) * f));
      ctx.lineTo(lx, ly);
    }
    ctx.stroke();
    ctx.textBaseline = 'top';
    ctx.fillText(`frq: ${
      pad(min.toFixed(), 3)
    } - ${
      pad(max.toFixed(), 3)
    } | ${
      pad((avg / bufferLength).toFixed(), 3)
    }`, x, 0);

    min = 0;
    max = 0;
    avg = 0;
    // eslint-disable-next-line
    ctx.strokeStyle = ctx.fillStyle = '#66D9EF';
    ctx.beginPath();
    for (i = 0; i < bufferLength; i += 1) {
      val = volume[i];
      avg += val;
      min = Math.min(min, val);
      max = Math.max(max, val);

      a = ((rad / bufferLength) * i) - Math.PI;
      td = (r / 100) * (val * 0.5);
      lx = Math.round(x + (Math.cos(a) * td));
      ly = Math.round(y + (Math.sin(a) * td));
      ctx.lineTo(lx, ly);
    }
    ctx.stroke();
    ctx.textBaseline = 'bottom';
    ctx.fillText(`vol: ${
      pad(min.toFixed(), 3)
    } - ${
      pad(max.toFixed(), 3)
    } | ${
      pad((avg / bufferLength).toFixed(), 3)
    }`, x, height - 30);

    if (this.centerDist <= r) {
      const dist = r * 0.005;
      // ctx.setLineDash([5, 5]);
      ctx.strokeStyle = 'rgba(255, 255, 255, 0.75)';
      ctx.beginPath();
      ctx.arc(x, y, this.centerDist, 0, Math.PI * 2);
      ctx.closePath();
      ctx.stroke();
      ctx.setLineDash([]);

      ctx.strokeStyle = 'black';
      ctx.fillStyle = 'white';
      ctx.textBaseline = 'bottom';
      ctx.textAlign = 'left';
      const mv = ((this.centerDist || 0) / dist).toFixed();
      ctx.strokeText(mv, this.mouseX, this.mouseY);
      ctx.fillText(mv, this.mouseX, this.mouseY);
    }


    requestAnimationFrame(this.update);
  }

  handleChange(evt) {
    const { target } = evt;
    const { value, name } = target;
    const update = {
      [name]: Number(value),
    };
    this.setState(update);
    this.props.audio.updateSettings(update);
  }

  handleBpmTab(evt) {
    evt.preventDefault();
    const tap = performance.now();
    this.taps = this.taps || [];
    let lastTap = this.taps.length && this.taps[this.taps.length - 1];

    if (lastTap && tap - lastTap > 2000) {
      this.taps = [];
      lastTap = false;
    }

    if (this.taps.length >= 3) {
      const stamps = this.taps
        .map(ts => ts - this.taps[0])
        .filter(v => v);
      const diffs = stamps
        .reduce((acc, val, i) => {
          if (!i) return [];
          acc.push(val - stamps[i - 1]);
          return acc;
        }, []);
      const bpm = 60000 / arrayAvg(diffs);
      this.props.onBpmChange(bpm);
    }

    this.taps.push(tap);
  }

  handleBpmInpChange(evt) {
    evt.preventDefault();
    const bpm = parseInt(evt.target.value, 10);
    this.props.onBpmChange(bpm);
  }

  render() {
    const {
      minDecibels,
      maxDecibels,
      smoothingTimeConstant,
      fftSize,
    } = this.state;

    const { bpm } = this.props;

    return (
      <div className="audio-manager d-flex flex-column">
        <form className="audio-manager__controls">
          <AudioManagerInput
            label="Min"
            properties={{
              name: 'minDecibels',
              onChange: this.handleChange,
              min: -200,
              max: -11,
              value: minDecibels,
            }}
          />
          <AudioManagerInput
            label="Max"
            properties={{
              name: 'maxDecibels',
              onChange: this.handleChange,
              min: -70,
              max: 120,
              value: maxDecibels,
            }}
          />
          <AudioManagerInput
            label="Smoothing"
            properties={{
              name: 'smoothingTimeConstant',
              onChange: this.handleChange,
              step: 0.01,
              min: 0,
              max: 1,
              value: smoothingTimeConstant,
            }}
          />
          <div className="form-group row">
            <label
              htmlFor="fftSize"
              className="col-form-label col-form-label-sm col-3"
            >
              Sampling
            </label>
            <div className="col-7">
              <select
                className="form-control form-control-sm"
                onChange={this.handleChange}
                id="fftSize"
                name="fftSize"
                value={fftSize}
              >
                <option value="32">32</option>
                <option value="64">64</option>
                <option value="128">128</option>
                <option value="256">256</option>
                <option value="1024">1024</option>
                <option value="2048">2048</option>
              </select>
            </div>
          </div>
        </form>

        <div className="audio-manager__bottom">
          <div className="audio-manager__bottom-left">
            <div className="audio-manager__monitor">
              <canvas ref={(el) => { this.canvas = el; }} />
            </div>
          </div>

          <div className="audio-manager__bottom-right">
            <div className="audio-manager__bpm-wrapper">
              <button
                className="audio-manager__bpm"
                onClick={this.handleBpmTab}
              >
                {bpm.toFixed()}
              </button>

              <input
                onClick={this.handleBpmTab}
                type="number"
                min="1"
                max="600"
                step="1"
                className="form-control"
                placeholder={bpm}
                onChange={this.handleBpmInpChange}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AudioManager.propTypes = {
  onBpmChange: PropTypes.func.isRequired,
  audio: PropTypes.object.isRequired,
  bpm: PropTypes.number.isRequired,
};
