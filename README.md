# Fiha

A complete rewrite of https://github.com/zeropaper/visual-fiha VJing tool.

![strong, black](https://img.shields.io/badge/%E2%98%95-strong%20black-brightgreen.svg?longCache=true)
![works on my machine](https://img.shields.io/badge/works-on%20my%20machine-brightgreen.svg?longCache=true)
![VW tests](https://img.shields.io/badge/VW%20tests-pass-brightgreen.svg?longCache=true)
[![Join the community on Spectrum](https://withspectrum.github.io/badge/badge.svg)](https://spectrum.chat/visual-fiha)

## Status

**This project is in its early stage.**  
It is also my first attempt to create a fairly complex app with React.js and Redux.

## Install

- Clone the repository
- Install the npm dependencies (`npm i`)
- Start the development build with `npm run dev`

## Usage

See [src/index.md](src/index.md).

## NPM Scripts

- `npm run dev`: starts the front-end and back-end servers
  in development mode (reload when changes are detected)
- `npm run build`: compiles the front-end for static serving
- `npm run live`: for live performance (build in production
  mode without JS minifiation)

## License

MIT, see the [LICENSE](./LICENSE) file at the root of the repository.
