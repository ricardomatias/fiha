import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import PropTypes from 'prop-types';
import React from 'react';
import { FihaMIDI, MIDIMapping } from '../services/midi/midi';

export function cleanClassName(str) {
  return str.toLowerCase().split(/[^a-z0-9]/g).join('-');
}

export function MIDIInput({
  input,
}) {
  const manufacturer = cleanClassName(input.device.manufacturer);
  const name = cleanClassName(input.device.name);
  const className = `midi__device midi__device--${manufacturer}--${name}`;
  return <div className={className}>{input.device.name.replace('MIDI 1', '')}</div>;
}

MIDIInput.propTypes = {
  input: PropTypes.instanceOf(MIDIMapping).isRequired,
};


export default function MIDIDevices({
  midi,
}) {
  const { inputs } = midi;
  const tabs = inputs.map(input => <Tab key={input.device.name}>{input.device.name}</Tab>);
  const panels = inputs.map(input => (
    <TabPanel key={input.device.name}>
      <MIDIInput input={input} />
    </TabPanel>
  ));

  return (
    <Tabs className="vertical-tabs">
      <TabList>
        {tabs}
      </TabList>

      {panels}
    </Tabs>
  );
}

MIDIDevices.propTypes = {
  midi: PropTypes.instanceOf(FihaMIDI).isRequired,
};
