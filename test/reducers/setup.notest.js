/* eslint-disable */
import deepFreeze from 'deep-freeze';
import setupReducer from '../../src/controller/reducers/setup';

describe('setup reducer', () => {
  it('adds a layer', () => {
    const stateBefore = {
      layers: [],
      signals: [],
    };

    const action = {
      type: 'ADD_LAYER',
      payload: {
        id: 'layer-1',
        prop: 'val',
      },
    };

    const stateAfter = {
      layers: [
        {
          id: 'layer-1',
          prop: 'val',
        },
      ],
      signals: [],
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(setupReducer(stateBefore, action)).toEqual(stateAfter);
  });


  it('edits a layer', () => {
    const stateBefore = {
      layers: [
        {
          id: 'layer-1',
          prop: 'val',
        },
      ],
      signals: [],
    };

    const action = {
      type: 'EDIT_LAYER',
      payload: {
        id: 'layer-1',
        prop: 'newval',
      },
    };

    const stateAfter = {
      layers: [
        {
          id: 'layer-1',
          prop: 'newval',
        },
      ],
      signals: [],
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(setupReducer(stateBefore, action)).toEqual(stateAfter);
  });


  it('removes a layer', () => {
    const stateBefore = {
      layers: [
        {
          id: 'layer-1',
          prop: 'val',
        },
      ],
      signals: [],
    };

    const action = {
      type: 'REMOVE_LAYER',
      payload: {
        id: 'layer-1',
      },
    };

    const stateAfter = {
      layers: [],
      signals: [],
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(setupReducer(stateBefore, action)).toEqual(stateAfter);
  });
});
