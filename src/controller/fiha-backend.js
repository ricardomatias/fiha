import storage from './fiha-storage';
import configStorage from './fiha-config-storage';
import GistService from './services/gist';
import defaultSetup from '../default-setup.json';

let started = performance.now();
let workerNow = started;
let currentSetup;
let signals = [];
const audio = {};
const gistService = new GistService(configStorage);

function updateTime() {
  const before = workerNow;
  workerNow = performance.now();

  signals = {
    ...signals,
    now: workerNow - started,
    'worker-frame-duration': workerNow - before,
  };
}

function resetSignals() {
  started = performance.now();
  signals = {
    bpm: signals.bpm || 120,
    started,
    now: 0,
    'worker-frame-duration': 0,
  };
  updateTime();
}

// eslint-disable-next-line
function socketPayload2Signals(payload) {
  const returned = {};
  // Object.keys(payload.data).forEach((name) => {
  // });
  return returned;
}

export default function (worker) {
  // eslint-disable-next-line no-param-reassign
  worker.broadcastChannel = new BroadcastChannel('fiha');

  const broadcast = data => worker.broadcastChannel.postMessage(data);

  const emit = data => worker.postMessage(data);

  const notifySuccess = message => emit({
    type: 'NOTIFICATION',
    payload: {
      type: 'success',
      message,
    },
  });
  const notifyFailure = error => emit({
    type: 'NOTIFICATION',
    payload: {
      type: 'error',
      message: error.message,
    },
  });

  const sendSetup = (data) => {
    resetSignals();
    currentSetup = data;

    emit({
      type: 'REPLACE_SETUP',
      payload: data,
    });

    broadcast({
      type: 'UPDATE_DATA',
      payload: {
        signals,
        audio,
      },
    });

    broadcast({
      type: 'REPLACE_LAYERS',
      payload: data.layers,
    });
  };

  const sendDefault = () => {
    resetSignals();
    currentSetup = defaultSetup;

    broadcast({
      type: 'UPDATE_DATA',
      payload: {
        signals,
        audio,
      },
    });

    broadcast({
      type: 'REPLACE_LAYERS',
      payload: defaultSetup.layers,
    });

    emit({
      type: 'REPLACE_SETUP',
      payload: defaultSetup,
    });
  };

  const broadcastReducer = (action) => {
    if (action.type === 'EXECUTION_ERROR') {
      const main = action.payload.stack.split('at eval');
      const error = main.shift().trim();
      const linecol = main[0].split('<anonymous>:')[1].split(')')[0];


      // eslint-disable-next-line
      let [line, col] = linecol.split(':').map(v => parseInt(v, 10));
      line -= action.payload.lineCorrection;
      const { id, role } = action.payload;

      emit({
        type: 'EXECUTION_ERROR',
        payload: {
          col,
          line,
          error,
          id,
          role,
        },
      });
    } else if (action.type === 'REGISTER_DISPLAY') {
      if (!currentSetup && action.payload.id === 'controller') {
        sendDefault();
        return;
      }

      broadcast({
        type: 'BROADCAST_FRAME',
        payload: {
          signals,
          audio,
        },
      });

      broadcast({
        type: 'REPLACE_LAYERS',
        payload: currentSetup.layers,
      });
    }
  };

  const setBroadcastId = (id) => {
    /* eslint-disable no-param-reassign */
    worker.broadcastChannel.close();
    worker.broadcastChannel = new BroadcastChannel(id);
    worker.broadcastChannel.addEventListener('message', evt => broadcastReducer(evt.data));
    /* eslint-enable no-param-reassign */
    notifySuccess(`Broadcasting on ${id}`);
  };

  const actionCallback = (action, data = {}) => {
    if (!action.payload || !action.payload.f_callbackId) return;
    emit({
      type: 'WORKER_CALLBACK',
      payload: {
        ...data,
        f_callbackId: action.payload.f_callbackId,
      },
    });
  };

  const workerReducer = (action) => {
    switch (action.type) {
      case 'SET_BPM':
        // eslint-disable-next-line no-console
        console.log('bpm changed', action.payload.bpm);
        signals.bpm = action.payload.bpm;
        break;

      case 'STORAGE_SAVE':
        storage.setItem(action.payload.id, action.payload)
          .then(() => {
            emit({
              type: 'REPLACE_SETUP',
              payload: action.payload,
            });
            actionCallback(action, {});
            notifySuccess(`${action.payload.id} saved`);
          })
          .catch(notifyFailure);
        break;

      case 'STORAGE_LOAD':
        storage.getItem(action.payload.id)
          .then((found) => {
            if (!found) {
              sendDefault();
              return;
            }

            sendSetup({
              setup: {
                id: action.payload.id,
              },
              ...found,
            });

            actionCallback(action, { ...found });
            notifySuccess(`${action.payload.id} loaded`);
          })
          .catch(notifyFailure);
        break;

      case 'GIST_CREDENTIALS':
        gistService
          .credentials(action.payload)
          .then(() => notifySuccess(action.payload && action.payload.token
            ? 'credentials saved'
            : 'credentials removed'))
          .catch(notifyFailure);
        break;

      case 'GIST_SAVE':
        gistService
          .save(action.payload)
          .then((response) => {
            notifySuccess(`Gist "${response.id}" saved`);
            actionCallback(action, response);
          })
          .catch(notifyFailure);
        break;

      case 'GIST_LOAD':
        gistService
          .load(action.payload.id)
          .then((loaded) => {
            notifySuccess(`Gist "${action.payload.id}" loaded`);
            actionCallback(action, loaded);
            // sendSetup(loaded);
          })
          .catch(notifyFailure);
        break;

      case 'SET_BROADCAST_ID':
        setBroadcastId(action.payload.id);
        actionCallback(action, {});
        break;

      case 'APPLY_SETUP':
        sendSetup(action.payload);
        actionCallback(action, {});
        notifySuccess('Setup applied');
        break;

      case 'BROADCAST':
        broadcast(action.payload);
        break;

      case 'UPDATE_DATA':
        signals = {
          ...signals,
          ...action.payload,
        };
        break;

      default:
    }
  };

  setInterval(() => {
    updateTime();
    broadcast({
      type: 'UPDATE_DATA',
      payload: signals,
    });
  }, 8);

  worker.addEventListener('message', evt => workerReducer(evt.data));

  const socketIoClientLib = process.env.FIHA_SOCKET_CLIENT;
  if (!socketIoClientLib) return;

  try {
    worker.importScripts(`${socketIoClientLib}/socket.io.slim.js`);

    // eslint-disable-next-line no-undef
    const socket = io(socketIoClientLib);

    socket.on('NEW_REMOTE', (payload) => {
      emit({
        type: 'NEW_REMOTE',
        payload,
      });
    });

    socket.on('REMOVE_REMOTE', (payload) => {
      emit({
        type: 'REMOVE_REMOTE',
        payload,
      });
    });

    socket.on('UPDATE_REMOTE', (payload) => {
      signals = {
        ...signals,
        ...socketPayload2Signals(payload),
      };
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(`socket script import failed, no available server at ${socketIoClientLib}`);
  }
}
