export default function setupReducer(state = {
  broadcastChannelId: 'fiha',
  animate: true,
  id: 'default-setup',
  edit: {
    role: '',
    id: '',
    type: '',
    value: '',
  },
}, action) {
  switch (action.type) {
    case 'SET_KEY':
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };
    case 'TOGGLE_KEY':
      return {
        ...state,
        [action.payload.key]: !state[action.payload.key],
      };

    case 'EDIT_CODE':
      return {
        ...state,
        edit: action.payload,
      };
    default:
      return state;
  }
}
