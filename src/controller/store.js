import configureStore from './configure-store';

const initialSetup = localStorage.fiha ? JSON.parse(localStorage.fiha) : false;
const store = configureStore(initialSetup);
store.subscribe(() => {
  localStorage.fiha = JSON.stringify(store.getState().setup);
});

export default store;
