/* eslint-disable */
import { render, mount } from 'enzyme';
import React from 'react';
import { CanvasDisplayLayer } from '../../src/display/components/scriptable-display-layer';


describe('CanvasDisplayLayer', () => {
  let mounted;
  let instance;

  beforeEach(() => {
    const layers = [];
    const props = {
      width: 400,
      height: 300,
    };

    mounted = mount(<CanvasDisplayLayer {...props} />);
    instance = mounted.instance();

    mounted.setProps({
      setup: `// this should provoke an "undefined" error
              // if one of the arguments is not passed
              (() => {})(ctx, utils, readSignal, cache);
              // console.info('evaled typeof', typeof cache);
              cache.setup = (cache.setup || 0) + 1;
              return cache;`,
      animation: `// this should provoke an "undefined" error
                  // if one of the arguments is not passed
                  (() => {})(ctx, utils, readSignal, cache);
                  cache.animation = (cache.animation || 0) + 1;
                  return cache;`,
    });
  });

  it('renders the DOM', () => {
    expect(instance.setupFunction).toBeInstanceOf(Function);
    expect(instance.animationFunction).toBeInstanceOf(Function);
  });

  it('executes the setup script', () => {
    instance.setupFunction = jest.fn(instance.setupFunction);
    instance.animationFunction = jest.fn(instance.animationFunction);

    expect(instance.cache).toEqual({ setup: 1 });

    // execSetup call will increase "setup" and "animation" by 1
    instance.execSetup();
    // this call will increase only "setup" by 1
    const result = instance.setupFunction(instance.ctx, {}, instance.readSignal, instance.cache);
    const setupCalls = instance.setupFunction.mock.calls;

    expect(setupCalls).toHaveLength(2);
    expect(result)
      .toEqual({ setup: 3 });
  });

  it('executes the animation script', () => {
    instance.setupFunction = jest.fn(instance.setupFunction);
    instance.animationFunction = jest.fn(instance.animationFunction);

    instance.execAnimation();
    expect(instance.animationFunction(instance.ctx, {}, instance.readSignal, instance.cache))
      .toEqual({ setup: 1, animation: 2 });

    const animationCalls = instance.animationFunction.mock.calls;
    expect(animationCalls).toHaveLength(2);
  });
});
