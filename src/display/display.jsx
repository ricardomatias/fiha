import querystring from 'querystring';
import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import DisplayLayers from './components/display-layers';

let qs;
const rootEl = document.getElementById('root');

const load = () => (render(
  <AppContainer>
    <DisplayLayers id={qs.did || 'main'} broadcastChannelId={qs.bcid || 'fiha'} />
  </AppContainer>,
  rootEl,
));

const handleHash = () => {
  qs = querystring.parse(window.location.search);
  rootEl.className = 'fiha fiha--display d-flex flex-column';
  load();
};

window.addEventListener('hashchange', handleHash);

if (module.hot) {
  module.hot.accept(load);
}

handleHash();
