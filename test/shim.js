global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};

global.performance = {
  now: () => process.hrtime()[1] * 0.000000001,
};

global.BroadcastChannel = class {
  constructor(id) {
    this.id = id;
    this.close = jest.fn();
    this.postMessage = jest.fn();
    this.addEventListener = jest.fn();
  }
};

/* eslint-disable */
global.AudioContext = class {
  constructor() {
    // console.info('new AudioContext', this.createAnalyser);
  }

  createAnalyser() {
    return {
      frequencyBinCount: 256,
    };
  }
};
/* eslint-enable */
