// http://esprima.readthedocs.io/en/latest/syntactic-analysis.html?highlight=parse
import { parseScript } from 'esprima';

const noop = () => {};

const prologue = 'let global, frame, window, top, document, history;\n';

export function toCode(body, ...argumentNames) {
  return `(${argumentNames.join(', ')}) => {
    'use strict';
    ${prologue}
    //
    ${body}
  }`;
}

// TODO: find a way to detect undefined variables at validation
export function validateCode(body, ...argumentNames) {
  const code = toCode(body, ...argumentNames);
  try {
    parseScript(code, {});
  } catch (syntaxErr) {
    syntaxErr.lineNumber -= 5;
    return syntaxErr;
  }
  return null;
}

export default function functionCompiler(
  body,
  ifNotCompiled = noop,
  onExecutionError,
  ...argumentNames // eslint-disable-line
) {
  let compiled;
  let printedError = false;

  const code = toCode(body, ...argumentNames);

  const syntaxErr = validateCode(body, ...argumentNames);
  if (syntaxErr) {
    compiled = ifNotCompiled;
  } else {
    try {
      // eslint-disable-next-line
      compiled = (eval(code)).bind(null);
    } catch (evalError) {
      // eslint-disable-next-line no-console
      console.log('Evaluation error', evalError.message);
      compiled = ifNotCompiled;
    }
  }

  return (...args) => {
    let result;

    try {
      result = compiled(...args);
      printedError = false;
    } catch (execErr) {
      if (!printedError) {
        printedError = true;
        const lineCorrection = code.split('\n').length - body.split('\n').length - 1;
        onExecutionError(execErr, lineCorrection);
      }
      result = execErr;
    }

    return result;
  };
}
