const reference = {};

const PI2 = Math.PI * 2;
reference.PI2 = {
  type: 'number',
  description: 'Shortcut to Math.PI * 2',
};

const sDiv = (val, div) => val * (1 / div);
reference.sDiv = {
  type: 'function',
  category: 'math',
  description: 'Error safe division (by avoiding: 0 / x)',
  usage: 'const dividedX = sDiv(x, y);',
};

const arrayMax = arr => arr.reduce((val, prev) => Math.max(val, prev), 0);
reference.arrayMax = {
  type: 'function',
  category: 'math',
  description: 'Get the maximal value in a array',
  usage: 'const maxVal = arrayMax([1, 3, 4]); // maxVal will be 4',
};

const arrayMin = arr => arr.reduce((val, prev) => Math.min(val, prev), 0);
reference.arrayMin = {
  type: 'function',
  category: 'math',
  description: 'Get the minimal value in a array',
  usage: 'const minVal = arrayMin([1, 3, 4]); // minVal will be 1',
};

const arraySum = arr => arr.reduce((val, prev) => val + prev, 0);
reference.arraySum = {
  type: 'function',
  category: 'math',
  description: 'Get the sum of array values',
  usage: 'const sumVal = arraySum([1, 3, 4]); // sumVal will be 8',
};

const arrayDiff = arr => (Math.abs(arrayMax(arr) - arrayMin(arr)));
reference.arrayDiff = {
  type: 'function',
  category: 'math',
  description: 'Get the difference between the maximal and minimal values in a array',
  usage: 'const diffVal = arrayDiff([1, 2, 4]); // diffVall will be 3',
};

const arrayAvg = arr => sDiv(arraySum(arr), arr.length);
reference.arrayAvg = {
  type: 'function',
  category: 'math',
  description: '',
  usage: '',
};

const arraySmooth = (arr, factor = 2) => arr.reduce((acc, val, index) => {
  acc.push(arrayAvg(arr.slice(index, index + factor)));
  return acc;
}, []);
reference.arraySmooth = {
  type: 'function',
  category: 'math',
  description: 'Smooth the values of an array (useful with audio)',
  snippet: `const smthFrq = arraySmooth(
  read('frequencies', []),
  10
);`,
};

const deg2rad = deg => (PI2 / 360) * deg;
reference.deg2rad = {
  type: 'function',
  category: 'math',
  description: '',
  usage: '',
};

const rad2deg = rad => (360 / PI2) * rad;
reference.rad2deg = {
  type: 'function',
  category: 'math',
  description: '',
  usage: '',
};

const cap = (val, min = 0, max = 127) => Math.min(Math.max(val, min), max);
reference.cap = {
  type: 'function',
  category: 'math',
  description: '',
  usage: '',
};

const between = (val, min = 0, max = 127) => val < max && val > min;
reference.between = {
  type: 'function',
  category: 'math',
  description: '',
  usage: '',
};

const beat = (now, bpm = 120) => {
  const timeBetweenBeats = (60 / bpm) * 1000;
  return (now % timeBetweenBeats) / timeBetweenBeats;
};
reference.beat = {
  type: 'function',
  category: 'math',
  description: '',
  usage: '',
};


const tools = {
  PI2,
  arrayMax,
  arrayMin,
  arraySum,
  arrayDiff,
  arrayAvg,
  arraySmooth,
  deg2rad,
  rad2deg,
  sDiv,
  cap,
  between,
  beat,
};

Object.getOwnPropertyNames(Math)
  .forEach((key) => {
    tools[key] = Math[key];
    const type = typeof Math[key];
    reference[key] = {
      type,
      category: 'math',
      description: `Equivalent of Math.${key}${type === 'function' ? '()' : ''}`,
      // eslint-disable-next-line
      link: `https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/${key}`,
    };
  });


export const apiReference = reference;

export default tools;
