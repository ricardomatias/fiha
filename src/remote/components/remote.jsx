import React from 'react';
import io from 'socket.io-client'; // eslint-disable-line
import throttle from 'lodash.throttle';
import './remote.scss';

const {
  addEventListener,
  removeEventListener,
  requestAnimationFrame,
} = window;

export default class RemoteUI extends React.Component {
  constructor(props) {
    super(props);

    const fps = 1000 / 60;
    this.devicemotionListener = throttle(this.devicemotionListener.bind(this), fps);
    this.mousemoveListener = throttle(this.mousemoveListener.bind(this), fps);
    this.keyupListener = throttle(this.keyupListener.bind(this), fps);
    this.touchmoveListener = throttle(this.touchmoveListener.bind(this), fps);
    this.motion = {};
    this.touches = [];

    this.emit = this.emit.bind(this);
    this.drawTouch = this.drawTouch.bind(this);
    this.animate = this.animate.bind(this);
    this.refCanvas = this.refCanvas.bind(this);

    this.initSocket();
    this.animate();
  }

  componentDidMount() {
    // eslint-disable-next-line no-console
    console.log('componentDidMount');
    if (this.socket) {
      this.socket.connect();
    }
    // addEventListener('touchmove', this.touchmoveListener);
    [
      'devicemotion',
      'mousemove',
      'keyup',
      'touchmove',
      'touchend',
    ].forEach(name => addEventListener(name, this[`${name}Listener`]));
  }

  componentWillUnmount() {
    // eslint-disable-next-line no-console
    console.log('componentWillUnmount');
    if (this.socket) {
      this.socket.disconnect();
    }
    // removeEventListener('touchmove', this.touchmoveListener);
    [
      'devicemotion',
      'mousemove',
      'keyup',
      'touchmove',
      'touchend',
    ].forEach(name => removeEventListener(name, this[`${name}Listener`]));
  }

  emit(action) {
    if (!this.socket) return;
    this.socket.emit(action.type, action.payload);
  }

  initSocket() {
    if (!process.env.FIHA_SOCKET_CLIENT) return;
    this.socket = io(process.env.FIHA_SOCKET_CLIENT, { autoConnect: false });
  }

  refCanvas(el) {
    if (el) {
      /* eslint-disable no-param-reassign */
      el.width = el.parentNode.clientWidth;
      el.height = el.parentNode.clientHeight;
      /* eslint-enable no-param-reassign */
      this.ctx = el.getContext('2d');
    } else {
      this.ctx = null;
    }
  }

  drawTouch(touch) {
    const { ctx } = this;
    ctx.beginPath();
    ctx.arc(touch.x, touch.y, 20, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
  }

  animate() {
    if (!this.ctx) {
      requestAnimationFrame(this.animate);
      return;
    }

    const { ctx } = this;
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.fillStyle = 'white';
    this.touches.forEach(this.drawTouch);
    requestAnimationFrame(this.animate);
  }

  devicemotionListener(evt) {
    const round = val => Math.round(val * 1000) / 1000;
    const changes = {
      x: round(evt.accelerationIncludingGravity.x || 0),
      y: round(evt.accelerationIncludingGravity.y || 0),
      z: round(evt.accelerationIncludingGravity.z || 0),
      alpha: round(evt.rotationRate.alpha || 0),
      beta: round(evt.rotationRate.beta || 0),
      gamma: round(evt.rotationRate.gamma || 0),
    };
    if (this.motion === changes) return;
    this.motion = changes;
    this.emit({
      type: 'UPDATE_REMOTE',
      payload: {
        motion: changes,
      },
    });
  }

  mousemoveListener(evt) { // eslint-disable-line
    // eslint-disable-next-line no-console
    console.log('mousemoveListener', ...Object.keys(evt));
  }

  keyupListener(evt) { // eslint-disable-line
    // eslint-disable-next-line no-console
    console.log('keyupListener', ...Object.keys(evt));
  }

  touchmoveListener(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    const changes = [];
    for (let t = 0; t < evt.touches.length; t += 1) {
      const touch = evt.touches.item(t);
      changes.push({
        id: touch.identifier,
        x: touch.pageX,
        y: touch.pageY,
      });
    }
    if (this.touches === changes) return;
    this.touches = changes;
    this.emit({
      type: 'UPDATE_REMOTE',
      payload: {
        touches: changes,
      },
    });
  }

  touchendListerner() {
    this.touches = [];
    this.emit({
      type: 'UPDATE_REMOTE',
      payload: {
        touches: [],
      },
    });
  }

  render() {
    return [
      <div key="overlay" className="overlay">
        <h1>RemoteUI!</h1>
        <div>
          Server:&nbsp;
          <a href={process.env.FIHA_SOCKET_CLIENT}>{process.env.FIHA_SOCKET_CLIENT}</a>
        </div>
        <div>ID: {this.socket.id}</div>
      </div>,
      <canvas key="canvas" ref={this.refCanvas} />,
    ];
  }
}

