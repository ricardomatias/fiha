export default class KeyboardService {
  constructor({
    dispatch = () => {},
    active = true,
  }) {
    this.active = active;
    this.dispatch = dispatch;
    this.listener = this.listener.bind(this);

    this.listenerOptions = {
      capture: false,
      passive: true,
    };

    this.values = {};

    this.listen();
  }

  listen() {
    document.body
      .addEventListener('keydown', this.listener, this.listenerOptions);
    document.body
      .addEventListener('keyup', this.listener, this.listenerOptions);
  }

  destroy() {
    document.body
      .removeEventListener('keydown', this.listener, this.listenerOptions);
    document.body
      .removeEventListener('keyup', this.listener, this.listenerOptions);
  }

  listener(evt) {
    const editor = document.getElementById('brace-editor');
    const inEditor = editor && editor.contains(evt.target);
    if (!inEditor && this.active) {
      const signature = [
        evt.key,
        evt.ctrlKey ? 'c' : null,
        evt.shiftKey ? 's' : null,
        evt.altKey ? 'a' : null,
      ]
        .filter(v => v)
        .map(v => v.toLowerCase(v))
        .reduce((uniques, val) => {
          if (!uniques.includes(val)) uniques.push(val);
          return uniques;
        }, [])
        .join('-');

      if (this.values[signature] === evt.type) return;
      this.values[signature] = evt.type;

      this.dispatch({
        type: 'UPDATE_DATA',
        payload: {
          [signature]: evt.type === 'keydown' ? 127 : 0,
        },
      });
    }
  }
}
