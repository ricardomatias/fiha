import functionCompiler from '../src/display/function-compiler';

describe('functionCompiler', () => {
  it('compiles valid code', () => {
    // eslint-disable-next-line
    const body = 'return `arg1: ${arg1}, arg2: ${arg2}`';
    const defaultFunc = () => {};
    const onExecutionError = () => {};
    const args = ['arg1', 'arg2'];
    const compiled = functionCompiler(body, defaultFunc, onExecutionError, ...args);
    expect(typeof compiled).toEqual('function');
    expect(compiled('a', 'b')).toEqual('arg1: a, arg2: b');
  });

  it('compiles invalid code to logger', () => {
    const body = 'b00m*!?!?';
    const defaultFunc = () => {};
    const args = ['arg1', 'arg2'];
    const compiled = functionCompiler(body, defaultFunc, ...args);
    expect(typeof compiled).toEqual('function');
    expect(compiled('a', 'b')).toEqual(undefined);
  });

  describe('compiled function', () => {
    it('cannot access "this"', () => {
      const compiled = functionCompiler('return this');
      const result = compiled();
      expect(result).toEqual(undefined);
    });

    [
      'global',
      'top',
      'window',
      'frame',
      'document',
      'history',
    ].forEach((name) => {
      it(`cannot access the ${name} object`, () => {
        const compiled = functionCompiler(`return ${name}`);
        const result = compiled();
        expect(result).toEqual(undefined);
      });
    });
  });
});
