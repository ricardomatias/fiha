const path = require('path');
const fs = require('fs');
const { EnvironmentPlugin } = require('webpack');
const marked = require('marked');
const uglify = require('uglifyjs-webpack-plugin');
const production = process.env.NODE_ENV === 'production';
const manifestJSON = require('./src/static/manifest.json');
const devPort = 8080;
const absPath = (dest) => path.join(__dirname, dest);
const read = file => fs.readFileSync(absPath(file), 'utf8');

module.exports = {
  options: {
    port: devPort,

    output: 'public',

    mains: {
      index: 'index',
      'remote/index': 'remote/remote',
      'controller/index': 'controller/controller',
      'display/index': 'display/display',
      'embedded/index': 'embedded/embedded',
    },
  },

  use: [
    [
      '@neutrinojs/react',
      {
        devServer: {
          host: '0.0.0.0',
          port: devPort,
        },

        style: {
          test: /\.(s|)css$/,
          loaders: ['sass-loader'],
        },

        html: {
          title: 'A VJing tool with MIDI support based on HTML5.',
          mobile: true,
          lang: 'en',
          template: absPath('src/index.ejs'),
          favicon: absPath('src/static/favicon.png'),
          appMountHtmlSnippet: options => `<div id="root-loading">${read('src/static/vf.icon-animated.svg')}</div>`,
          headHtmlSnippet: options => (`<style>${read('src/atf.css')}</style>`).replace(/[\n]+[\s]*/g, ''),
          bodyHtmlSnippet: (options) => {
            if (options.filename === 'index.html') {
              return `
              <div class="container">
                <div class="row">
                  <div class="col">
                    ${marked(read('src/index.md'))}
                  </div>
                </div>
              </div>`;
            }
            return '';
          },
          meta: [
            {
              name: 'theme-color',
              content: manifestJSON.theme_color,
            }
          ],
          links: [
            {
              href: './static/manifest.json',
              rel: 'manifest',
            }
          ]
        },

        clean: {
          exclude: ['.git']
        },

        babel: {
          env: {
            production: {
              plugins: [
                'babel-plugin-transform-react-remove-prop-types',
              ]
            }
          }
        },
      },
    ],

    (neutrino) => {
      const { config } = neutrino;

      if (!production) {
        config
          .plugin('env')
          .use(EnvironmentPlugin, [
            {
              FIHA_SOCKET_CLIENT: 'http://localhost:9090'
            }
          ]);
      }

      config.plugins
        .delete('babel-minify')
      ;
      if (production && !process.env.FIHA_QUICK_BUILD) {
        config
          .plugin('uglify')
          .use(uglify)
          .before('optimize-css')
        ;
      }

      config
        .entry('vendor')
          .add('react')
          .add('react-dom')
          .add('paper/dist/paper-full')
          .add('three')
          .add('redux')
        ;
    },

    // [
    //   '@neutrinojs/airbnb',
    //   {
    //     include: [
    //       absPath('src'),
    //       absPath('src/fiha-backend.js'),
    //       absPath('test'),
    //     ],
    //     exclude: [
    //       absPath('node_modules'),
    //     ],
    //     eslint: {
    //       envs: ['es6', 'browser'],
    //       rules: {
    //         'object-curly-newline': 'off',
    //         'jsx-a11y/label-has-for': 'off',
    //         'jsx-a11y/click-events-have-key-events': 'off',
    //         'jsx-a11y/no-static-element-interactions': 'off',
    //         'jsx-a11y/anchor-is-valid': 'off',
    //         'react/forbid-prop-types': 'off',
    //       },
    //     },
    //   }
    // ],

    [
      '@neutrinojs/jest',
      {
        setupFiles: [
          absPath('test/shim.js'),
          absPath('test/jest.setup.js'),
        ],
      },
    ],
  ],

  env: {
    NODE_ENV: {
      production: {
        use: [
          // TODO: configure the SW properly
          // so it only caches embedded related assets
          // '@neutrinojs/pwa',
          [
            '@neutrinojs/airbnb',
            {
              include: [
                absPath('src'),
                absPath('src/fiha-backend.js'),
                // absPath('test'),
              ],
              exclude: [
                absPath('node_modules'),
              ],
              eslint: {
                envs: ['es6', 'browser'],
                rules: {
                  'object-curly-newline': 'off',
                  'jsx-a11y/label-has-for': 'off',
                  'jsx-a11y/click-events-have-key-events': 'off',
                  'jsx-a11y/no-static-element-interactions': 'off',
                  'jsx-a11y/anchor-is-valid': 'off',
                  'react/forbid-prop-types': 'off',
                },
              },
            },
          ]
        ],
      },
      // development: {
      //   use: [
      //   ]
      // },
      test: (neutrino) => neutrino.config.module
        .rule('compile')
        .use('babel')
        .tap(options => {
          // console.info('jest options', options);
          options.env.test = options.env.test || { plugins: [] };
          options.env.test.plugins.push('babel-plugin-dynamic-import-node');
          return options;
        })
    }
  }
};
