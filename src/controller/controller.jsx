import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import FihaRedux from './fiha-redux';

const rootEl = document.getElementById('root');

const load = () => (render(
  <AppContainer>
    <FihaRedux />
  </AppContainer>,
  rootEl,
));

const handleHash = () => {
  rootEl.className = 'fiha fiha--controller d-flex flex-column';
  load();
};

window.addEventListener('hashchange', handleHash);

if (module.hot) {
  module.hot.accept(load);
}

handleHash();
