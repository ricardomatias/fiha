import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
} from 'react-sortable-hoc';
import './control-layers.scss';

import ThreeDisplayLayer from '../../display/components/three-display-layer';
import CanvasDisplayLayer from '../../display/components/canvas-display-layer';
import PaperDisplayLayer from '../../display/components/paper-display-layer';

const types = {
  three: ThreeDisplayLayer,
  canvas: CanvasDisplayLayer,
  paper: PaperDisplayLayer,
};

const ControlLayerHandle = () => (<span className="btn btn-light drag-handle">↕</span>);

// eslint-disable-next-line babel/new-cap
const SortableControlLayerHandle = SortableHandle(ControlLayerHandle);

export function ControlLayer({
  id,
  edit,
  type,
  active,
  handleRemoveLayer,
  handleEditLayer,
  handleEditCode,
  focused = false,
}) {
  const htmlId = `ctrl-layer-${id}`;
  const focusedClass = focused ? ' control-layer--focused' : '';
  const activeClass = active ? ' control-layer--active' : '';
  const className = `row py-1 control-layer control-layer--${type}${focusedClass}${activeClass}`;
  return (
    <li className={className}>
      <div className="col-2">
        <button
          className="btn btn-sm btn-danger"
          onClick={() => handleRemoveLayer(id)}
        >
          Remove
        </button>
      </div>

      <div className="col control-layer__type text-center">{type}</div>

      <div className="col-3 control-layer__id">
        <div className="custom-control custom-checkbox custom-control-inline">
          <input
            id={htmlId}
            className="custom-control-input"
            type="checkbox"
            defaultChecked={active}
            onChange={() => handleEditLayer({
              id,
              active: !active,
            })}
          />
          <label
            htmlFor={htmlId}
            title="Automatically apply changes"
            className="custom-control-label"
          >
            {id}
          </label>
        </div>
      </div>

      <div className="col text-right">
        <span className="btn-group btn-group-sm">
          <SortableControlLayerHandle />

          <button
            className={`btn btn-${edit && edit === 'setup' ? 'warning' : 'default'}`}
            onClick={() => handleEditCode({
              id,
              type,
              role: 'setup',
            })}
          >
            Setup
          </button>

          <button
            className={`btn btn-${edit && edit === 'animation' ? 'warning' : 'default'}`}
            onClick={() => handleEditCode({
              id,
              type,
              role: 'animation',
            })}
          >
            Animation
          </button>
        </span>
      </div>
    </li>
  );
}

ControlLayer.propTypes = {
  id: PropTypes.string.isRequired,
  edit: PropTypes.string,
  type: PropTypes.string.isRequired,
  active: PropTypes.bool,
  handleRemoveLayer: PropTypes.func.isRequired,
  handleEditLayer: PropTypes.func.isRequired,
  handleEditCode: PropTypes.func.isRequired,
  focused: PropTypes.bool,
};

ControlLayer.defaultProps = {
  edit: '',
  active: true,
  focused: false,
};

// eslint-disable-next-line babel/new-cap
export const SortableLayer = SortableElement(ControlLayer);

export function ControlLayersList({
  edit,
  layers,
  handleEditLayer,
  handleRemoveLayer,
  handleEditCode,
}) {
  return (
    <ul className="control-layers__list">
      {layers.map((layer, index) => (
        <SortableLayer
          key={layer.id}
          index={index}
          {...layer}
          edit={layer.id === edit.id ? edit.role : ''}
          handleEditLayer={handleEditLayer}
          handleRemoveLayer={handleRemoveLayer}
          handleEditCode={handleEditCode}
        />
      ))}
    </ul>
  );
}

ControlLayersList.propTypes = {
  edit: PropTypes.object.isRequired,
  layers: PropTypes.array.isRequired,
  handleEditLayer: PropTypes.func.isRequired,
  handleRemoveLayer: PropTypes.func.isRequired,
  handleEditCode: PropTypes.func.isRequired,
};

// eslint-disable-next-line babel/new-cap
export const SortableLayers = SortableContainer(ControlLayersList);


export default class ControlLayers extends Component {
  constructor(props) {
    super(props);
    this.onSortEnd = this.onSortEnd.bind(this);
  }

  onSortEnd(info) {
    this.props.handleSortLayers(info.oldIndex, info.newIndex);
  }

  render() {
    const {
      handleAddLayer,
    } = this.props;

    return (
      <section className="control-layers container-fluid">
        <form className="py-3 control-layers__add-form">
          <div className="input-group">
            <input placeholder="layer-id" name="layerId" className="form-control form-control-sm" type="text" />
            <select name="type" className="form-control form-control-sm control-layers__add-type">
              {Object.keys(types).map(type => (
                <option
                  key={type}
                  value={type}
                >
                  {type}
                </option>
              ))}
            </select>
            <button
              className="btn btn-sm btn-primary input-group-addon control-layers__add-button"
              onClick={(evt) => {
                evt.preventDefault(evt);
                const parent = evt.target.parentNode;
                const val = name => parent.querySelector(`[name=${name}]`).value;
                handleAddLayer(val('type'), val('layerId'));
              }}
            >
              Add layer
            </button>
          </div>
        </form>
        <SortableLayers
          {...this.props}
          onSortEnd={this.onSortEnd}
          useDragHandle
          axis="y"
          lockAxis="y"
        />
      </section>
    );
  }
}

ControlLayers.propTypes = {
  layers: PropTypes.array.isRequired,
  edit: PropTypes.object,
  handleAddLayer: PropTypes.func.isRequired,
  handleEditLayer: PropTypes.func.isRequired,
  handleSortLayers: PropTypes.func.isRequired,
  handleRemoveLayer: PropTypes.func.isRequired,
  handleEditCode: PropTypes.func,
};

ControlLayers.defaultProps = {
  edit: {},
  handleEditCode: () => {},
};
