const fs = require('fs');
const path = require('path');
const http = require('http');

const clientPath = path.dirname(require.resolve('socket.io-client/dist/socket.io'));

const app = http.createServer((req, res) => {
  fs.readFile(`${clientPath}/${req.url}`, (err, data) => {
    if (err) {
      res.writeHead(500);
      res.end(err.message);
    } else {
      res.setHeader('ContentType', 'application/javascript');
      res.writeHead(200);
      res.end(data);
    }
  });
});
const io = require('socket.io')(app);

let workerSocket;

const sendWorker = (action) => {
  if (!workerSocket) return;
  workerSocket.emit(action.type, action.payload);
};

const workerConnection = (socket) => {
  if (workerSocket) {
    workerSocket.removeAllListeners();
  }
  workerSocket = socket;
};


const remoteConnection = (socket) => {
  sendWorker({
    type: 'NEW_REMOTE',
    payload: {
      id: socket.id,
    },
  });

  socket.on('NEW_REMOTE', (payload) => {
    sendWorker('NEW_REMOTE', payload);
  });

  socket.on('UPDATE_REMOTE', (payload) => {
    sendWorker({
      type: 'UPDATE_REMOTE',
      payload: {
        data: payload,
        id: socket.id,
      },
    });
  });
};

const isRemote = referer => referer.includes('remote');

io.on('connection', (socket) => {
  if (isRemote(socket.handshake.headers.referer)) {
    remoteConnection(socket);
  } else {
    workerConnection(socket);
  }
});


app.listen(9090);
