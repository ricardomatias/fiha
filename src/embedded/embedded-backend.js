import { json2setup } from '../controller/services/gist';

let started = performance.now();
let workerNow = started;
let signals = [];
// eslint-disable-next-line
const audio = {};

function updateTime() {
  const before = workerNow;
  workerNow = performance.now();

  signals = {
    ...signals,
    now: workerNow - started,
    'worker-frame-duration': workerNow - before,
  };
}

function resetSignals() {
  started = performance.now();
  signals = {
    started,
    now: 0,
    'worker-frame-duration': 0,
  };
  updateTime();
}

export default function (worker) {
  // eslint-disable-next-line no-param-reassign
  worker.broadcastChannel = new BroadcastChannel('fiha');

  const broadcast = data => worker.broadcastChannel.postMessage(data);

  const emit = data => worker.postMessage(data);

  const notifySuccess = (message) => {
    emit({
      type: 'NOTIFICATION',
      payload: {
        type: 'success',
        message,
      },
    });
  };
  const notifyFailure = (error) => {
    emit({
      type: 'NOTIFICATION',
      payload: {
        type: 'error',
        message: error.message,
      },
    });
  };

  const setBroadcastId = (id) => {
    /* eslint-disable no-param-reassign */
    worker.broadcastChannel.close();
    worker.broadcastChannel = new BroadcastChannel(id);
    /* eslint-enable no-param-reassign */
    notifySuccess(`Broadcasting on ${id}`);
  };

  const sendSetup = (data) => {
    resetSignals();

    broadcast({
      type: 'UPDATE_DATA',
      signals,
      audio,
    });

    broadcast({
      type: 'REPLACE_LAYERS',
      payload: data.layers,
    });
  };

  const actionCallback = (action, data = {}) => {
    if (!action.payload || !action.payload.f_callbackId) return;
    emit({
      type: 'WORKER_CALLBACK',
      payload: {
        ...data,
        f_callbackId: action.payload.f_callbackId,
      },
    });
  };

  const workerReducer = (action) => {
    switch (action.type) {
      case 'SET_BROADCAST_ID':
        setBroadcastId(action.payload.id);
        actionCallback(action, {});
        break;

      case 'SETUP_USE':
        sendSetup(action.payload);
        break;

      case 'LOAD_GIST':
        fetch(`https://api.github.com/gists/${action.payload.id}`)
          .then(response => response.json())
          .then(json2setup)
          .then((loaded) => {
            notifySuccess(`Gist "${action.payload.id}" loaded`);
            actionCallback(action, loaded);
            sendSetup(loaded);
          })
          .catch(notifyFailure);
        break;

      case 'BROADCAST':
        broadcast(action.payload);
        break;

      case 'UPDATE_DATA':
        signals = {
          ...signals,
          ...action.payload,
        };
        break;

      default:
    }
  };

  setInterval(() => {
    updateTime();
    broadcast({
      type: 'UPDATE_DATA',
      payload: signals,
    });
  }, 8);

  worker.addEventListener('message', evt => workerReducer(evt.data));
}
