import React from 'react';
import PropTypes from 'prop-types';

export default class StorageItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: null,
      loading: false,
    };
  }

  render() {
    const {
      id,
      load,
      loadCode,
      save,
    } = this.props;
    const { code, loading } = this.state;

    const loadingLabel = loading ? 'Loading' : 'Inspect';
    const inspectLabel = code ? 'Close inspector' : loadingLabel;
    const inspectCloseClick = () => {
      this.setState({ code: null });
    };
    const inspectClick = () => {
      if (this.state.code) {
        inspectCloseClick();
        return;
      } else if (loading) {
        return;
      }
      this.setState({ loading: true });
      // eslint-disable-next-line no-console
      console.log('inspect', id);
      loadCode(id)((err, loadedCode) => {
        if (err) {
          this.setState({ code: err.stack, loading: false });
          return;
        }
        this.setState({ code: loadedCode, loading: false });
      });
    };

    const loadClick = () => {
      load(id);
    };
    const saveClick = () => {
      save(id);
    };

    return (
      <li className="row storage-list__item">
        <div className="col">
          <div className="row">
            <div className="col flex-grow-0">
              <button disabled className="btn btn-danger">Delete</button>
            </div>
            <div className="col col-form-label storage-list__id">{id}</div>
            <div className="col flex-grow-0">
              <button className="btn btn-default storage-list__inspect" onClick={inspectClick}>{inspectLabel}</button>
            </div>
            <div className="col btn-group d-flex flex-grow-0 justify-content-end storage-list__actions">
              <button className="btn btn-default storage-list__load" onClick={loadClick}>Load</button>
              <button className="btn btn-primary storage-list__save" onClick={saveClick}>Save</button>
            </div>
          </div>

          {(
            code && (
              <div className="row">
                <div className="col">
                  <pre>{code}</pre>
                </div>
              </div>
            )
          )}
        </div>
      </li>
    );
  }
}

StorageItem.propTypes = {
  id: PropTypes.string.isRequired,
  load: PropTypes.func,
  loadCode: PropTypes.func,
  save: PropTypes.func,
};

StorageItem.defaultProps = {
  load: () => {},
  loadCode: (cb) => { cb(new Error('Missing loadCode property')); },
  save: () => {},
};
