import React from 'react';
import PropTypes from 'prop-types';
import functionCompiler from '../function-compiler';

const {
  addEventListener,
  removeEventListener,
} = window;

export default class ScriptableDisplayLayer extends React.Component {
  constructor(props) {
    super(props);

    this.initialize = this.initialize.bind(this);
    this.setCanvas = this.setCanvas.bind(this);
    this.reset = this.reset.bind(this);
    this.compileSetup = this.compileSetup.bind(this);
    this.execSetup = this.execSetup.bind(this);
    this.compileAnimation = this.compileAnimation.bind(this);
    this.execAnimation = this.execAnimation.bind(this);
    this.handleResize = this.handleResize.bind(this);

    this.cache = {};
    this.offCanvas = document.createElement('canvas');
    this.read = props.read;
  }

  componentDidMount() {
    addEventListener('resize', this.handleResize, {
      capture: false,
      passive: true,
    });

    this.props.register(this.props.id, this);

    this.reset();
  }

  componentWillReceiveProps(nextProps) {
    const {
      setup,
      animation,
    } = this.props;

    const setupChanged = nextProps.setup !== setup;
    const animationChanged = nextProps.animation !== animation;

    if (animationChanged) {
      this.compileAnimation(nextProps.animation);
    }

    if (setupChanged) {
      this.compileSetup(nextProps.setup);
      this.reset();
    }
  }

  shouldComponentUpdate(nextProps) {
    return !this.canvas ||
      this.props.scale !== nextProps.scale;
  }

  componentDidUpdate() {
    this.handleResize();
  }

  componentWillUnmount() {
    removeEventListener('resize', this.handleResize, {
      capture: false,
      passive: true,
    });

    this.props.register(this.props.id);
  }

  get setupArgumentNames() {
    return [...this.setupArgNames, 'read', 'cache'];
  }

  get animationArgumentNames() {
    return [...this.animationArgNames, 'read', 'cache'];
  }

  get setupArguments() {
    return this.setupArgumentNames.map(name => this[name]);
  }

  get animationArguments() {
    return this.animationArgumentNames.map(name => this[name]);
  }

  setCanvas(canvas) {
    if (!canvas) return;
    this.canvas = canvas;
  }

  reset() {} // eslint-disable-line class-methods-use-this

  initialize() {
    this.compileSetup();
    this.compileAnimation();
  }

  compileSetup(script = this.props.setup) {
    this.setupFunction = functionCompiler(
      script,
      this.setupFunction,
      this.props.onExecutionError('setup', this.props.id),
      ...this.setupArgumentNames // eslint-disable-line
    );
  }

  execSetup() {
    const {
      setupFunction,
      setupArguments,
    } = this;
    setupFunction(...setupArguments);
  }

  compileAnimation(script = this.props.animation) {
    this.animationFunction = functionCompiler(
      script,
      this.animationFunction,
      this.props.onExecutionError('animation', this.props.id),
      ...this.animationArgumentNames // eslint-disable-line
    );
  }

  execAnimation() {
    const {
      offCanvas,
      destinationCtx,
      canvas,
      animationFunction,
      animationArguments,
    } = this;
    const { width, height } = canvas;
    animationFunction(...animationArguments);

    if (destinationCtx) {
      canvas.width = width;
      destinationCtx.drawImage(offCanvas, 0, 0, width, height, 0, 0, width, height);
    }
  }

  handleResize() {
    const { canvas, offCanvas } = this;
    if (!canvas) return;

    const width = canvas.parentNode.clientWidth;
    const height = canvas.parentNode.clientHeight;

    const scale = Math.max(Math.min(1, this.props.scale || 0.5), 0.5);
    offCanvas.width = width * scale;
    offCanvas.height = height * scale;
    canvas.width = width * scale;
    canvas.height = height * scale;
    canvas.style.transform = `scale(${1 / scale})`;

    this.ctx = offCanvas.getContext('2d');
    this.destinationCtx = canvas.getContext('2d');
  }

  render() {
    const {
      id,
      width,
      height,
      type,
    } = this.props;
    const className = `display-layer display-layer--${type}`;

    return (
      <canvas
        id={id}
        ref={this.setCanvas}
        className={className}
        width={width}
        height={height}
      />
    );
  }
}

ScriptableDisplayLayer.propTypes = {
  onExecutionError: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  animation: PropTypes.string,
  height: PropTypes.number,
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  setup: PropTypes.string,
  read: PropTypes.func.isRequired,
  width: PropTypes.number,
  scale: PropTypes.number.isRequired,
};

ScriptableDisplayLayer.defaultProps = {
  animation: '',
  height: 150,
  setup: '',
  width: 400,
};
