import backend from '../src/controller/fiha-backend';

const workerMock = () => {
  const listeners = [];
  const messages = [];
  return {
    addEventListener: jest.fn((name, fn) => {
      listeners.push([name, fn]);
    }),
    postMessage: jest.fn((data) => {
      messages.push(data);
    }),
    importScript: jest.fn(),
    terminate: jest.fn(),
    mockMessage: (data) => {
      listeners.forEach((info) => {
        if (info[0] !== 'message') return;
        info[1]({ data });
      });
    },
    getMockedMessages: () => messages,
  };
};

// probably overkilled
const waitFor = (condition, timeout = 1000) => new Promise((resolve, reject) => {
  let started;
  const interval = setInterval(() => {
    started = started || Date.now();
    const now = Date.now();
    if (now - started >= timeout) {
      reject(new Error(`${timeout}ms Timeout Exceeded`));
      clearInterval(interval);
      return;
    }

    if (condition()) {
      resolve();
      clearInterval(interval);
    }
  }, 1000 / 60);
});

describe('backend', () => {
  let bound;
  beforeEach(() => {
    bound = workerMock();
    backend(bound);
  });

  it('binds the worker', () => {
    expect(bound.addEventListener.mock.calls).toHaveLength(1);
    expect(bound.postMessage.mock.calls).toHaveLength(0);
  });

  describe('action', () => {
    describe('STORAGE_SAVE', () => {
      it('saves a setup', async () => {
        expect(() => {
          bound.mockMessage({
            type: 'STORAGE_SAVE',
          });
        }).toThrow();

        bound.mockMessage({
          type: 'STORAGE_SAVE',
          payload: {
            id: 'test-id',
          },
        });
        expect(bound.addEventListener.mock.calls).toHaveLength(1);
        expect(bound.postMessage.mock.calls).toHaveLength(0);
        await waitFor(() => bound.postMessage.mock.calls.length);
        expect(bound.postMessage.mock.calls).toHaveLength(1);

        const lastMessage = bound.postMessage.mock.calls[0][0];
        expect(lastMessage.type).toBe('NOTIFICATION');

        // localForage in node environment...
        // expect(lastMessage.resultType).not.toBe('error');
      });
    });


    describe('STORAGE_LOAD', () => {

    });


    describe('SET_BROADCAST_ID', () => {
      it('requires a payload with id', () => {
        expect(() => {
          bound.mockMessage({
            type: 'SET_BROADCAST_ID',
          });
        }).toThrow();

        expect(() => {
          bound.mockMessage({
            type: 'SET_BROADCAST_ID',
            payload: {
              id: 'test-id',
            },
          });
        }).not.toThrow();

        expect(bound.addEventListener.mock.calls).toHaveLength(1);
        expect(bound.postMessage.mock.calls).toHaveLength(1);
      });
    });


    describe('BROADCAST', () => {
      it('broadcasts', async () => {
        expect(() => {
          bound.mockMessage({
            type: 'BROADCAST',
            payload: {
              test: 'test',
            },
          });
        }).not.toThrow();
        const lastBroadcast = bound.broadcastChannel.postMessage.mock.calls[0][0];
        expect(lastBroadcast).toBeTruthy();
        expect(lastBroadcast).toEqual({ test: 'test' });
      });
    });


    describe('BROADCAST_FRAME', () => {
      it('broadcasts a frame', async () => {
        expect(() => {
          bound.mockMessage({
            type: 'BROADCAST_FRAME',
            payload: {
              test: 'test',
            },
          });
        }).not.toThrow();
        // eslint-disable-next-line no-console
        // console.info('bound', bound);
        // expect(lastBroadcast).toBeTruthy();
        // expect(lastBroadcast).toHaveProperty('type', 'BROADCAST_FRAME');
        // expect(lastBroadcast).toHaveProperty('payload.now');
        // expect(lastBroadcast).toHaveProperty('payload.test', 'test');

        // const firstNow = lastBroadcast.payload.now;
        // expect(() => {
        //   bound.mockMessage({
        //     type: 'BROADCAST_FRAME',
        //     payload: {
        //       test: 'overridden',
        //       newProp: 'added'
        //     }
        //   });
        // }).not.toThrow();

        // lastBroadcast = bound.broadcastChannel.postMessage.mock.calls[1][0];
        // expect(lastBroadcast).toBeTruthy();
        // expect(lastBroadcast).toHaveProperty('type', 'BROADCAST_FRAME');
        // expect(lastBroadcast).toHaveProperty('payload.now');
        // expect(lastBroadcast.payload.now).toBeGreaterThan(firstNow);
        // expect(lastBroadcast).toHaveProperty('payload.test', 'overridden');
        // expect(lastBroadcast).toHaveProperty('payload.newProp', 'added');
      });
    });


    describe('UPDATE_DATA', () => {
      it('updates data', async () => {
        expect(() => {
          bound.mockMessage({
            type: 'UPDATE_DATA',
            payload: {
              test: 'test',
            },
          });
        }).not.toThrow();
      });
    });
  });
});
