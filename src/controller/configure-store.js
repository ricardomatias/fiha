import { createStore, combineReducers } from 'redux';
import { connect } from 'react-redux';
import rootReducer from './reducers';
import {
  handleAddLayer,
  handleEditLayer,
  handleRemoveLayer,
  handleSortLayers,
} from './actions/layers';
import {
  toggleAnimation,
  handleEditCode,
} from './actions/setup';


export const mapStateToProps = state => ({
  ...state.setup,
  setup: state.setup,
  layers: state.layers,
});

export const mapDispatchToProps = dispatch => ({
  handleAddLayer: (...args) => {
    dispatch(handleAddLayer(...args));
  },
  handleEditLayer: (...args) => {
    dispatch(handleEditLayer(...args));
  },
  handleEditCode: (...args) => {
    dispatch(handleEditCode(...args));
  },
  handleRemoveLayer: (...args) => {
    dispatch(handleRemoveLayer(...args));
  },
  handleSortLayers: (...args) => {
    dispatch(handleSortLayers(...args));
  },
  toggleAnimation: (...args) => {
    dispatch(toggleAnimation(...args));
  },
  replaceSetup: (info) => {
    dispatch({
      type: 'SET_KEY',
      payload: {
        key: 'id',
        value: info.id,
      },
    });
    // dispatch({
    //   type: 'SET_KEY',
    //   payload: {
    //     key: 'edit',
    //     value: {},
    //   },
    // });
    dispatch({
      type: 'REPLACE_LAYERS',
      payload: info.layers,
    });
  },
  actions: {
    layers: {},
    gist: {
      save: () => {
        // eslint-disable-next-line no-console
        console.log('gist.save');
      },
      load: () => {
        // eslint-disable-next-line no-console
        console.log('gist.load');
      },
    },
    forage: {
      save: () => {
        // eslint-disable-next-line no-console
        console.log('forage.save');
      },
      load: () => {
        // eslint-disable-next-line no-console
        console.log('forage.load');
      },
    },
    setup: {
      getCode: () => {
        // eslint-disable-next-line no-console
        console.log('getCode');
      },
      changeDescription: () => {
        // eslint-disable-next-line no-console
        console.log('changeDescription');
      },
    },
  },
});

export const connector = connect(mapStateToProps, mapDispatchToProps);

export default function (initialSetup) {
  const initial = initialSetup ? { setup: initialSetup } : undefined;
  const store = createStore(combineReducers(rootReducer), initial);

  if (module.hot) {
    module.hot.accept('./reducers/index', () => {
      // eslint-disable-next-line
      const newReducer = require('./reducers/index');
      store.replaceReducer(combineReducers(newReducer.default));
    });
  }

  return store;
}
