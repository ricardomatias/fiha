/* eslint-disable */
import deepFreeze from 'deep-freeze';
import signalsReducer from '../../src/controller/reducers/signals';

describe('signals reducer', () => {
  it('adds a signal', () => {
    const stateBefore = [];

    const action = {
      type: 'ADD_SIGNAL',
      id: 'signal-1',
      prop: 'val',
    };

    const stateAfter = [
      {
        id: 'signal-1',
        prop: 'val',
      },
    ];

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(signalsReducer(stateBefore, action)).toEqual(stateAfter);
  });


  it('edits a signal', () => {
    const stateBefore = [
      {
        id: 'signal-1',
        prop: 'val',
      },
    ];

    const action = {
      type: 'EDIT_SIGNAL',
      payload: {
        id: 'signal-1',
        prop: 'newval',
      },
    };

    const stateAfter = [
      {
        id: 'signal-1',
        prop: 'newval',
      },
    ];

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(signalsReducer(stateBefore, action)).toEqual(stateAfter);
  });


  it('removes a signal', () => {
    const stateBefore = [
      {
        id: 'signal-1',
        prop: 'val',
      },
    ];

    const action = {
      type: 'REMOVE_SIGNAL',
      payload: {
        id: 'signal-1',
      },
    };

    const stateAfter = [];

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(signalsReducer(stateBefore, action)).toEqual(stateAfter);
  });
});
