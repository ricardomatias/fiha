export default function (val) {
  return (100 / 127) * (val || 0);
}
