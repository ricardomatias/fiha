import React from 'react';

const noop = () => {};

export default function InputGroup({
  name,
  defaultValue,
  value,
  pattern,
  type,
  placeholder,
  onChange,
  ref,
  help,
  className,
  addons,
}) {
  let inputProps = {
    key: 'input',
    name,
    pattern,
    type,
    ref,
    placeholder,
    className: 'form-control',
  };

  if (defaultValue) {
    inputProps = {
      ...inputProps,
      defaultValue,
    };
  } else {
    inputProps = {
      ...inputProps,
      value,
      onChange,
    };
  }

  return [
    <div
      key="input"
      className={`input-group ${className || ''}`}
    >
      {[
        <input
          {...inputProps}
        />,
        ...addons.map(item => (
          <button
            key={item.text}
            type={item.type || 'button'}
            disabled={(typeof item.disabled === 'function' && item.disabled()) || item.disabled}
            className={`btn input-group-addon ${item.className || ''}`}
            onClick={item.onClick || noop}
          >
            {item.text}
          </button>
        )),
      ]}
    </div>,

    <small
      key="help"
      className="form-text"
    >
      {help}
    </small>,
  ];
}
