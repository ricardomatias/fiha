/* eslint-disable */
import { mount, render } from 'enzyme';
import React from 'react';
import Fiha from '../../src/components/fiha';

describe('Fiha', () => {
  it('does things', () => {
    const props = {
      workerInit: jest.fn(() => {}),
    };

    const rendered = render(<div><Fiha {...props} /></div>);
    expect(rendered.find('.toolbar')).toHaveLength(1);
    expect(rendered.find('.display-n-controls')).toHaveLength(1);
    expect(rendered.find('.display-layers')).toHaveLength(1);
    expect(rendered.find('.fiha-editor')).toHaveLength(1);
  });
});
