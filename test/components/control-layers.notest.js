/* eslint-disable */
import { mount, render } from 'enzyme';
import React from 'react';
import ControlLayers, { ControlLayer } from '../../src/controls/components/control-layers';

const makeProps = () => ({
  layers: [
    {
      id: 'l1',
      layerType: 'canvas',
      setup: 'console.info("setup only");',
    },
    {
      id: 'l2',
      layerType: 'canvas',
      setup: 'console.info("setup");',
      animation: 'console.info("animation");',
    },
    {
      id: 'l3',
      layerType: 'canvas',
      animation: 'console.info("animation only");',
    },
  ],
  handleAddLayer: jest.fn(() => {}),
  handleEditLayer: jest.fn(() => {}),
  handleRemoveLayer: jest.fn(() => {}),
  handleEditCode: jest.fn(() => {}),
});

describe('ControlLayers', () => {
  it('renders', () => {
    const props = makeProps();

    const rendered = render(<ControlLayers {...props} />);
    const layerElements = rendered.find('.control-layer');
    const addLayerButton = rendered.find('.control-layers__add-button');

    expect(layerElements).toHaveLength(3);
    expect(addLayerButton).toHaveLength(1);
  });

  it('adds a layer', () => {
    const props = makeProps();

    const mounted = mount(<ControlLayers {...props} />);
    const addLayerButton = mounted.find('.control-layers__add-button');

    addLayerButton.simulate('click');
    const newLayer = mounted.find('.control-layer');
    expect(newLayer).toHaveLength(3);

    const addCalls = props.handleAddLayer.mock.calls;
    expect(addCalls).toHaveLength(1);
    expect(addCalls[0][0]).toEqual('canvas');
  });
});

describe('ControlLayer', () => {
  it('renders', () => {

  });
});
