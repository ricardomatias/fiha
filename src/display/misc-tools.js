const reference = {};

reference.read = {
  type: 'function',
  category: 'misc',
  description: 'Returns a "signal" value or a default value',
  usage: `// get the audio frequencies
const frq = read('frequencies', []);
// get the precise time
const now = read('now', 0);`,
};

const noop = () => {};
reference.noop = {
  type: 'function',
  category: 'misc',
  description: 'Function that does not do anything',
};

const isFunction = what => typeof what === 'function';
reference.isFunction = {
  type: 'function',
  category: 'misc',
  description: 'Determine if something is a function',
};

const toggled = {};
const prevToggle = {};
const toggle = (read, name) => (on, off) => {
  const val = read(name);
  if (prevToggle[name] !== val && val) toggled[name] = !toggled[name];
  if (toggled[name] && isFunction(on)) on();
  if (!toggled[name] && isFunction(off)) off();
  prevToggle[name] = val;
  return toggled[name];
};
reference.toggle = {
  type: 'function',
  category: 'misc',
  description: 'Creates a function which can be used to execute 1 or 2 functions depending if a value is "on" or "off". See also inOut().',
  snippet: `// NOTE!
// you should actually use this function only in
// the SETUP script and keep its result in
// the cache object like:
//
// cache.keyA = toggle(read, 'a');
//
// and then in your ANIMATION script:
//
// cache.keyA(
//   () => { /* on */ },
//   () => { /* off */ }
// );`,
};

const inOut = (read, name) => (on, off) => {
  const val = read(name);
  if (val && isFunction(on)) on();
  if (!val && isFunction(off)) off();
  return val;
};
reference.inOut = {
  type: 'function',
  category: 'misc',
  description: 'Creates a function which can be used to execute 1 or 2 functions depending if a value is "on" or "off". See also toggle().',
  snippet: `// NOTE!
// you should actually use this function only in
// the SETUP script and keep its result in
// the cache object like:
//
// cache.keyA = inOut(read, 'a');
//
// and then in your ANIMATION script:
//
// cache.keyA(
//   () => { /* on */ },
//   () => { /* off */ }
// );`,
};

const tools = {
  noop,
  isFunction,
  toggle,
  inOut,
};

export const apiReference = reference;

export default tools;
