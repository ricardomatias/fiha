import querystring from 'querystring';
import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import EmbeddedDisplay from './components/embedded-display';

let qs;
const rootEl = document.getElementById('root');

const load = () => (render(
  <AppContainer>
    <EmbeddedDisplay
      gistId={qs.gid || '285879c5979e970df7665c7d7092947a'}
      broadcastChannelId={qs.bcid || `fiha-${(Math.random() * 10000).toFixed()}`}
    />
  </AppContainer>,
  rootEl,
));

const handleHash = () => {
  qs = querystring.parse(window.location.hash.slice(1));
  rootEl.className = 'fiha fiha--embedded';
  load();
};

window.addEventListener('hashchange', handleHash);

if (module.hot) {
  module.hot.accept(load);
}

handleHash();
