import React from 'react';
import PropTypes from 'prop-types';
import StorageItem from './storage-item';
import InputGroup from './input-group';

// eslint-disable-next-line
const warn = console.warn.bind(console);

export default class StorageForage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      id: '',
      keys: [],
    };

    this.save = this.save.bind(this);
    this.importCode = this.importCode.bind(this);
    this.list = this.list.bind(this);
    this.list();
  }

  list() {
    this.props.storage
      .keys()
      .then(keys => this.setState({ keys }))
      .catch(warn);
  }

  save(evt) {
    evt.preventDefault();
    this.props.save(this.state.id);
  }

  importCode(evt) {
    const { id, code } = this.state;
    evt.preventDefault();
    this.props.importCode(id, code)((...args) => {
      // eslint-disable-next-line no-console
      console.log('importCode', ...args);
    });
  }

  render() {
    const { code } = this.state;
    const { save, importCode } = this;
    const saveImportLabel = code ? 'Import' : 'Save';
    const localElements = [];

    const setStateValue = name => (evt) => {
      // eslint-disable-next-line no-console
      console.log('set', name, evt.target.checkValidity());
      const newState = {};
      newState[name] = evt.target.value.trim();
      this.setState(newState);
    };

    localElements.push((
      <form key="idform" className="container-fluid py-3">
        <div className="row mb-3">
          <div className="col">
            <InputGroup
              className="storage-new"
              onChange={setStateValue('id')}
              value={this.state.id}
              placeholder="storage-id"
              pattern="[a-zA-Z0-9_-]+"
              addons={[
                {
                  text: saveImportLabel,
                  onClick: evt => (code ? importCode(evt) : save(evt)),
                },
              ]}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <textarea
              onChange={setStateValue('code')}
              placeholder="JSON setup code"
              className="form-control"
            />
          </div>
        </div>
      </form>
    ));

    localElements.push((
      <ul key="list" className="container-fluid storage-list">
        {this.state.keys.map(key => (
          <StorageItem {...this.props} key={key} id={key} />
        ))}
      </ul>
    ));

    return localElements;
  }
}

StorageForage.propTypes = {
  storage: PropTypes.object.isRequired,
  load: PropTypes.func.isRequired,
  loadCode: PropTypes.func.isRequired,
  importCode: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
  close: PropTypes.func,
};

StorageForage.defaultProps = {
  close: () => {},
};
