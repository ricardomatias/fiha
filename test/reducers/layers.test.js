import deepFreeze from 'deep-freeze';
import layersReducer from '../../src/controller/reducers/layers';

describe('layers reducer', () => {
  it('adds a layer', () => {
    const stateBefore = [];

    const action = {
      type: 'ADD_LAYER',
      payload: {
        id: 'layer-1',
        prop: 'val',
      },
    };

    const stateAfter = [
      {
        id: 'layer-1',
        prop: 'val',
      },
    ];

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(layersReducer(stateBefore, action)).toEqual(stateAfter);
  });


  it('edits a layer', () => {
    const stateBefore = [
      {
        id: 'layer-1',
        prop: 'val',
      },
    ];

    const action = {
      type: 'EDIT_LAYER',
      payload: {
        id: 'layer-1',
        prop: 'newval',
      },
    };

    const stateAfter = [
      {
        active: true,
        animation: '',
        setup: '',
        id: 'layer-1',
        prop: 'newval',
      },
    ];

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(layersReducer(stateBefore, action)).toEqual(stateAfter);
  });


  it('removes a layer', () => {
    const stateBefore = [
      {
        id: 'layer-1',
        prop: 'val',
      },
    ];

    const action = {
      type: 'REMOVE_LAYER',
      payload: {
        id: 'layer-1',
      },
    };

    const stateAfter = [];

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(layersReducer(stateBefore, action)).toEqual(stateAfter);
  });
});
