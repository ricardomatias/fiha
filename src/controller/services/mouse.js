export default class MouseService {
  constructor({
    element = document.body,
    dispatch = () => {},
    active = true,
  }) {
    this.element = element;
    this.active = active;
    this.dispatch = dispatch;
    this.listen = this.listen.bind(this);
    this.listener = this.listener.bind(this);
    this.destroy = this.destroy.bind(this);

    this.listenerOptions = {
      capture: false,
      passive: true,
    };

    this.listen();
  }

  listen() {
    this.element
      .addEventListener('mousemove', this.listener, this.listenerOptions);
  }

  destroy() {
    this.element
      .removeEventListener('mousemove', this.listener, this.listenerOptions);
  }

  listener(evt) {
    const {
      clientWidth,
      clientHeight,
    } = this.element;

    this.dispatch({
      type: 'UPDATE_DATA',
      payload: {
        'mouse-x': (evt.offsetX / clientWidth) * 127,
        'mouse-y': (evt.offsetY / clientHeight) * 127,
      },
    });
  }
}
