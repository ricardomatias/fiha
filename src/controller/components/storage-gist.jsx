import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import GistService, { getGistId } from '../services/gist';
import storage from '../fiha-config-storage';
import InputGroup from './input-group';

// eslint-disable-next-line
const warn = console.warn.bind(console);

export const GistTokenUser = ({
  name,
  login,
  avatarUrl,
  clearToken,
}) => (
  <div className="container-fluid py-3">
    <div className="row">
      <div className="col-2">
        <img className="img-fluid" src={avatarUrl} alt="avatar" />
      </div>

      <div className="col d-flex flex-column justify-content-between">
        <div className="row">
          <div className="col">
            <div>{name}</div>
            <div>aka <code>{login}</code></div>
          </div>
        </div>

        <div className="row">
          <div className="col">
            <button
              className="btn btn-link"
              onClick={clearToken}
            >
              Clear token
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

GistTokenUser.propTypes = {
  name: PropTypes.string.isRequired,
  login: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  clearToken: PropTypes.func.isRequired,
};

export default class StorageGist extends React.Component {
  constructor(props) {
    super(props);

    this.mounted = false;
    this.state = {
      id: getGistId() || '',
      login: '',
      avatarUrl: '',
      name: '',
      description: '',
      current: null,
    };

    this.gistService = new GistService(storage);
    this.gistService.user
      .then(info => this.setState(info));

    this.setStateValue = this.setStateValue.bind(this);
    this.saveToken = this.saveToken.bind(this);
    this.clearToken = this.clearToken.bind(this);
    this.updateCurrent = this.updateCurrent.bind(this);
    this.renderHistory = this.renderHistory.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    const { id } = this.state;
    const { load } = this.props;
    if (id) load(id, this.updateCurrent);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  setStateValue(name) {
    return (evt) => {
      const newState = {};
      newState[name] = evt.target.value;
      this.setState(newState);
    };
  }

  saveToken(evt) {
    evt.preventDefault();
    if (!evt.target.checkValidity()) {
      evt.target.parentNode.classList.add('error');
      return;
    }
    evt.target.parentNode.classList.remove('error');

    const token = evt.target.elements.item(0).value;
    if (!token) return;

    this.gistService.credentials({ token })
      .then(info => this.setState(info))
      .catch(() => this.setState({ login: '' }));
  }

  clearToken(evt) {
    evt.preventDefault();
    this.gistService.credentials(false)
      .then(() => this.setState({ login: '' }))
      .catch(() => this.setState({ login: '' }));
  }

  updateCurrent(response) {
    if (!this.mounted) return;
    this.setState({
      id: response.id,
      current: response,
      description: response.description,
    });
  }

  renderHistory(entry) {
    return (
      <li key={entry.version} className="py-1">
        <div className="d-flex justify-content-between align-items-baseline">
          <button
            className="btn btn-link mr-1"
            onClick={() => {
              this.props.load(`${this.state.id}/${entry.version}`, this.updateCurrent);
            }}
          >
            {moment(entry.updated).fromNow()}
          </button>
          <a href={`https://github.com/${entry.author}`} target="_blank">by {entry.author}</a>
        </div>
      </li>
    );
  }

  render() {
    const gistElements = [];
    const {
      id,
      description,
      current,

      login,
      avatarUrl,
      name,
    } = this.state;

    if (!login) {
      gistElements.push((
        <form
          key="gisttoken"
          className="container-fluid pb-3 pt-3"
          onSubmit={this.saveToken}
        >
          <div className="row">
            <div className="col">
              <InputGroup
                type="password"
                placeholder="Token"
                addons={[
                  {
                    type: 'submit',
                    text: 'Save token',
                  },
                ]}
                help={[
                  <div key="line-1">⚠ The token will saved locally (in your browser storage) ⚠</div>,
                  <div key="line-2">To generate a token:</div>,
                  <ul key="do">
                    <li>
                      <a
                        rel="noopener noreferrer"
                        target="_blank"
                        href="https://github.com/settings/tokens"
                      >
                        Go to the GitHub &quot;Personal access tokens&quot; page
                      </a>
                    </li>
                    <li>Click the &quot;Generate new token&quot;</li>
                    <li>Only select &quot;gist&quot; scope</li>
                  </ul>,
                ]}
              />
            </div>
          </div>
        </form>
      ));
    } else {
      gistElements.push((
        <GistTokenUser
          key="gistuser"
          name={name}
          login={login}
          avatarUrl={avatarUrl}
          clearToken={this.clearToken}
        />
      ));
    }

    const gistAddons = [];
    if (login) {
      gistAddons.push({
        className: 'btn-primary',
        onClick: () => this.props.save(id, description, this.updateCurrent),
        text: id ? 'Update' : 'Create',
      });
    }
    if (id) {
      gistAddons.push({
        className: 'btn-secondary',
        onClick: () => this.props.load(id, this.updateCurrent),
        text: 'Load',
      });
    }

    const helpTxt = `Leave empty to create a new Gist.
      Specify an ID if you want to load or update an existing Gist.
      If you are not the owner of the Gist,
      it will be forked and then updated.`;
    gistElements.push((
      <form key="gistform" className="container-fluid pb-3">
        <div className="row pb-3">
          <div className="col">
            <InputGroup
              placeholder="Gist ID"
              value={id}
              onChange={this.setStateValue('id')}
              addons={gistAddons}
              help={helpTxt}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <textarea
              className="form-control"
              placeholder="Description of the setup."
              value={description}
              rows={Math.min(description.split('\n').length, 10)}
              onChange={this.setStateValue('description')}
            />
          </div>
        </div>
      </form>
    ));

    if (current) {
      gistElements.push((
        <div key="current" className="container-fluid pb-3 flex-grow-1">
          <div className="row">
            <div className="col-4">
              <h3>History</h3>
              <ol reversed>
                {
                  (current.history || [])
                    .map(this.renderHistory)
                }
              </ol>
            </div>
            <div className="col">
              <h3>Layers</h3>
              <ul>
                {
                  (current.layers || [])
                    .map(layer => <li key={layer.id}>{layer.id} {layer.type}</li>)
                }
              </ul>
            </div>
          </div>
        </div>
      ));

      gistElements.push((
        <div key="apply" className="container-fluid pb-3">
          <div className="row">
            <div className="col">
              <button
                className="btn btn-primary"
                onClick={() => this.props.apply(current)}
              >
                Apply setup
              </button>
            </div>
          </div>
        </div>
      ));
    }

    return gistElements;
  }
}

StorageGist.propTypes = {
  apply: PropTypes.func.isRequired,
  load: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
};
