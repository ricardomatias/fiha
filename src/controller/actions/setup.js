export const toggleAnimation = () => ({
  type: 'TOGGLE_ANIMATION',
});

export const handleEditCode = payload => ({
  type: 'EDIT_CODE',
  payload,
});
