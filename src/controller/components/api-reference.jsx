import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';
import 'brace';
import 'brace/mode/javascript';
import 'brace/snippets/javascript';
import 'brace/theme/monokai';

import './api-reference.scss';


export const ReadOnlyAce = ({
  value,
  fontSize = 14,
  maxLines = 30,
  tabSize = 2,
}) => (<AceEditor
  className="api-reference__code"
  mode="javascript"
  theme="monokai"
  fontSize={fontSize}
  showGutter={false}
  showPrintMargin={false}
  maxLines={maxLines}
  tabSize={tabSize}
  highlightActiveLine
  wrapEnabled
  readOnly
  value={value}
  editorProps={{
    $blockScrolling: Infinity,
  }}
/>);

ReadOnlyAce.propTypes = {
  value: PropTypes.string,
  fontSize: PropTypes.number,
  maxLines: PropTypes.number,
  tabSize: PropTypes.number,
};

ReadOnlyAce.defaultProps = {
  value: '',
  fontSize: 14,
  maxLines: 30,
  tabSize: 2,
};


export const APIReferenceTopicItem = ({
  topic,
  addSnippet,
  showReference,
  description,
  focused,
  type,
  category,
}) => (
  <li
    className={`api-reference__list-item ${
      focused ? 'api-reference__list-item--focused' : ''
    }`}
  >
    <a
      className="api-reference__add-snippet-link"
      onClick={addSnippet}
      title="Add snippet in editor"
      href="#"
    >
      +
    </a>

    <span
      className={`api-reference__details-link api-reference__name api-reference__name--${type}`}
      onClick={showReference}
      title={description}
    >
      <span>{topic}</span>
      {type === 'function' && '()'}
    </span>
    {category && (
      <span
        className={`api-reference__category api-reference__category--${category}`}
      >
        {category}
      </span>
    )}
  </li>
);

APIReferenceTopicItem.propTypes = {
  showReference: PropTypes.func.isRequired,
  addSnippet: PropTypes.func.isRequired,
  topic: PropTypes.string.isRequired,
  description: PropTypes.string,
  focused: PropTypes.bool,
  type: PropTypes.string,
  category: PropTypes.string,
};

APIReferenceTopicItem.defaultProps = {
  description: '',
  focused: false,
  type: '',
  category: '',
};


export const APIReferenceTopicDetails = ({
  addSnippet,
  topic,
  usage,
  snippet,
  description,
  type,
  link,
}) => (
  <div className="api-reference__details">
    <div className="api-reference__meta">
      <span className={`api-reference__name api-reference__name--${type}`}>
        <span>{topic}</span>
        {type === 'function' ? '()' : ''}
      </span>

      {link && (
        link.indexOf('https://developer.mozilla.org') === 0 ?
        (
          <a
            href={link}
            className="api-reference__link api-reference__link--mdn"
            target="_blank"
            rel="noopener noreferrer"
            title={`MDN documentation for ${topic}`}
          >
            <span className="invisible">MDN documentation for {topic}</span>
          </a>
        ) :
        (
          <a
            href={link}
            className="api-reference__link"
            target="_blank"
            rel="noopener noreferrer"
            title={`Documentation for ${topic}`}
          >
            Docs
          </a>
        )
      )}
    </div>

    <div className="api-reference__description">{description}</div>

    {usage && (<ReadOnlyAce value={usage} />)}

    {snippet && snippet !== usage && (<ReadOnlyAce value={snippet} />)}

    {snippet && (
      <div key="button" className="api-reference__add-snippet-button">
        <button className="form-control form-control-sm" onClick={addSnippet}>
          Add snippet
        </button>
      </div>
    )}
  </div>
);

APIReferenceTopicDetails.propTypes = {
  topic: PropTypes.string.isRequired,
  addSnippet: PropTypes.func.isRequired,
  usage: PropTypes.string,
  snippet: PropTypes.string,
  description: PropTypes.string,
  type: PropTypes.string,
  link: PropTypes.string,
};

APIReferenceTopicDetails.defaultProps = {
  usage: '',
  snippet: '',
  description: '',
  type: '',
  link: '',
};


export default class APIReference extends Component {
  state = {
    searched: null,
    topic: null,
  };

  onSearchChange = evt => this.setState({
    searched: evt.target.value,
  });

  filtered = () => {
    const { searched } = this.state;
    const { apiReference } = this.props;
    return Object.keys(apiReference)
      .filter(item => item.indexOf(searched || '') > -1);
  };

  render = () => {
    const {
      apiReference,
      addSnippet,
    } = this.props;
    const topicInfo = apiReference[this.state.topic];

    return (
      <div className="api-reference">
        <input
          placeholder="Search API"
          className="api-reference__search-input form-control form-control-sm"
          type="search"
          onChange={this.onSearchChange}
        />
        <ul className="api-reference__list">
          {this.filtered().map(topic => (
            <APIReferenceTopicItem
              key={topic}
              focused={this.state.topic === topic}
              showReference={(evt) => {
                if (evt) evt.preventDefault();
                this.setState({ topic });
              }}
              addSnippet={addSnippet(topic)}
              topic={topic}
              {...apiReference[topic]}
            />
          ))}
        </ul>
        {topicInfo && (
          <APIReferenceTopicDetails
            {...topicInfo}
            addSnippet={addSnippet(this.state.topic)}
            topic={this.state.topic}
          />
        )}
      </div>
    );
  };
}

APIReference.propTypes = {
  apiReference: PropTypes.object.isRequired,
  addSnippet: PropTypes.func.isRequired,
};
