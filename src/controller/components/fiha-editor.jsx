import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';
import ace from 'brace';
import 'brace/mode/javascript';
import 'brace/snippets/javascript';
import 'brace/theme/monokai';
import 'brace/ext/language_tools';
import 'brace/ext/searchbox';
import APIReference from './api-reference';
import './fiha-editor.scss';

const {
  addEventListener,
  removeEventListener,
} = window;

const JSMode = ace.acequire('ace/mode/javascript').Mode;

class EditorMode extends JSMode {
  constructor() {
    super();
    this.createWorker = () => {};
  }
}
const langTools = ace.acequire('ace/ext/language_tools');

export default class FihaEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: 'javascript',
      theme: 'monokai',
      fontSize: 16,
      highlightActiveLine: true,
      wrapEnabled: true,
      autoApply: props.autoApply,
      value: props.value,
      editorProps: {
        $blockScrolling: Infinity,
        ...props.editorProps,
      },
      setOptions: {
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true,
        enableSnippets: false,
        showLineNumbers: true,
        displayIndentGuides: true,
        showInvisibles: true,
        tabSize: 2,
        ...props.setOptions,
      },
    };

    this.refEditor = this.refEditor.bind(this);
    this.toggleAutoApply = this.toggleAutoApply.bind(this);
    this.applyListener = this.applyListener.bind(this);
    this.handleApplyClick = this.handleApplyClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.addSnippet = this.addSnippet.bind(this);

    langTools.addCompleter(this.completer);
  }

  componentDidMount() {
    const customMode = new EditorMode();
    this.editor.getSession().setMode(customMode);
    addEventListener('keydown', this.applyListener);
  }

  componentWillReceiveProps(nextProps, nextState) {
    const { value } = nextProps;
    if (this.state.value !== value && nextState.value !== value) {
      this.setState({ value });
    }
  }

  componentDidUpdate() {
    if (this.editor && this.editor.resize) this.editor.resize();
  }

  componentWillUnmount() {
    removeEventListener('keydown', this.applyListener);
  }

  onChange(value) {
    this.setState({ value });
    if (!this.state.autoApply) return;
    const codeError = this.props.validateCode(value);

    this.setState({ codeError });
    if (codeError) return;

    this.setExecutionError();

    this.applyChange(value);
  }

  get completer() {
    return {
      getCompletions: (editor, session, pos, prefix, callback) => {
        const { codeGlobals, type } = this.props;
        callback(null, Object.keys(codeGlobals).map(name => ({
          name,
          value: name,
          score: 1000,
          meta: type,
        })));
      },
    };
  }

  setExecutionError(executionError) {
    this.setState({ executionError });
  }

  applyChange(value = this.state.value) {
    this.props.handleEditorChange({
      id: this.props.id,
      [this.props.role]: value,
    });
  }

  handleApplyClick(evt) {
    evt.preventDefault();
    this.applyChange();
  }

  applyListener(evt) {
    if (evt.ctrlKey
      && evt.key.toLowerCase() === 'enter') {
      evt.preventDefault();
      this.applyChange();
    }
  }

  refEditor(el) {
    if (el && el.editor) {
      this.editor = el.editor;
    }
  }

  toggleAutoApply() {
    this.setState({
      autoApply: !this.state.autoApply,
    });
  }

  addSnippet(topic) {
    const { editor } = this;
    const info = this.props.apiReference[topic];
    const { snippet } = info;

    return (evt) => {
      if (evt && evt.preventDefault) evt.preventDefault();

      const { ranges } = editor.getSelection();
      if (ranges && ranges.length) {
        editor.getSelection().ranges.forEach((range) => {
          editor.session.insert(range.cursor, snippet || topic);
        });
        return;
      }

      editor.session.insert(editor.getCursorPosition(), snippet || topic);
    };
  }

  render() {
    const { addSnippet, state, props } = this;
    const {
      autoApply,
      value,
      codeError,
      executionError,
      showHelp,
    } = state;
    const { role, type, id, apiReference } = props;
    const hasChanges = props.value !== value;
    let { className } = props;
    className += `fiha-editor${codeError ? ' fiha-editor--has-error' : ''}`;

    return (
      <div className={className}>
        <div className="fiha-editor__toolbar">
          <span className="fiha-editor__toolbar-id">{id}</span>
          <span className="fiha-editor__toolbar-type">
            {type}{' '}
            <a
              title={`Browse the ${type} API`}
              href="#"
              className="fiha-editor__toolbar-help"
              onClick={(evt) => {
                evt.preventDefault();
                this.setState({ showHelp: !showHelp });
              }}
            >
              🛈
            </a>
          </span>
          <span className="fiha-editor__toolbar-role">{role}</span>
        </div>

        <div className="fiha-editor__columns">
          {showHelp && (
            <APIReference
              addSnippet={addSnippet}
              apiReference={apiReference}
            />
          )}

          <div className="fiha-editor__column">
            <AceEditor
              className="fiha-editor__ace"
              ref={this.refEditor}
              key="editor"
              width="100%"
              height="100%"
              {...this.state}
              onChange={this.onChange}
            />
          </div>
        </div>

        {(codeError ? (
          <div className="fiha-editor__error fiha-editor__toolbar alert-danger">
            [Code] {codeError.description}, line: {codeError.lineNumber}
          </div>
        ) : '')}

        {(executionError ? (
          <div className="fiha-editor__error fiha-editor__toolbar alert-danger">
            <div>
              <small>{executionError.id} ({executionError.role})</small>
              <div>
                [Execution] {executionError.error},
                line {executionError.line},
                column: {executionError.col}
              </div>
            </div>
          </div>
        ) : '')}

        <form className="fiha-editor__status form-inline">
          <div>
            <button
              disabled={(autoApply || !hasChanges)}
              className="btn btn-primary"
              onClick={this.handleApplyClick}
              title="CTRL + ENTER"
            >
              Apply
            </button>
          </div>
          <div className="for-check ml-sm-2">
            <div className="custom-control custom-checkbox custom-control-inline">
              <input
                id="auto-apply-checkbox"
                className="custom-control-input"
                type="checkbox"
                onChange={this.toggleAutoApply}
                defaultChecked={autoApply}
              />
              <label
                htmlFor="auto-apply-checkbox"
                title="Automatically apply changes"
                className="custom-control-label"
              >
                auto
              </label>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

FihaEditor.propTypes = {
  autoApply: PropTypes.bool,
  value: PropTypes.string,
  role: PropTypes.string,
  type: PropTypes.string,
  id: PropTypes.string,
  editorProps: PropTypes.object,
  setOptions: PropTypes.object,
  handleEditorChange: PropTypes.func.isRequired,
  validateCode: PropTypes.func,
  className: PropTypes.string,
  codeGlobals: PropTypes.array,
  apiReference: PropTypes.object,
};

FihaEditor.defaultProps = {
  validateCode: () => {},
  autoApply: true,
  value: '',
  role: '',
  type: '',
  id: '',
  editorProps: {},
  setOptions: {},
  className: '',
  codeGlobals: [],
  apiReference: {},
};
