import React from 'react';
import { PortalWithState } from 'react-portal';
import PropTypes from 'prop-types';
import './toolbar.scss';

export function storageMenu({
  portalStyle,
  selectItem,
  closePortal,
}) {
  const clickHandler = (evt) => {
    selectItem(evt);
    closePortal();
  };

  return (
    <div key="storagemenu" style={portalStyle} className="toolbar-menu">
      <ul className="toolbar-menu__items">
        <li className="toolbar-menu__item">
          <a
            data-action="local"
            className="toolbar__button"
            onClick={clickHandler}
            title="CTRL + SHIFT + S"
          >
            Local DB…
          </a>
        </li>
        <li className="toolbar-menu__item">
          <a
            data-action="save"
            className="toolbar__button"
            onClick={clickHandler}
            title="CTRL + S"
          >
            Save
          </a>
        </li>
        <li className="toolbar-menu__item">
          <a
            data-action="revert"
            className="toolbar__button"
            onClick={clickHandler}
            title="CTRL + R"
          >
            Revert
          </a>
        </li>

        <li className="toolbar-menu__divider" />

        <li className="toolbar-menu__item">
          <a
            data-action="gist"
            className="toolbar__button"
            onClick={clickHandler}
            title="CTRL + SHIFT + G"
          >
            Gist…
          </a>
        </li>
      </ul>
    </div>
  );
}

storageMenu.propTypes = {
  portalStyle: PropTypes.object.isRequired,
  selectItem: PropTypes.func.isRequired,
  closePortal: PropTypes.func.isRequired,
};


export default class Toolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      top: 0,
      left: 0,
    };

    this.openStorageMenu = this.openStorageMenu.bind(this);
    this.selectStorageMenuItem = this.selectStorageMenuItem.bind(this);
  }

  openStorageMenu(openPortal) {
    return (evt) => {
      const toolbarBox = evt.target.closest('.toolbar').getBoundingClientRect();
      const itemBox = evt.target.getBoundingClientRect();
      this.setState({
        top: toolbarBox.bottom,
        left: itemBox.left,
      });
      openPortal(evt);
    };
  }

  selectStorageMenuItem(evt) {
    const link = evt.target;
    this.props.openStorageMenu(link.getAttribute('data-action'));
  }

  render() {
    const {
      scale,
      animate,
      toggleAnimation,
      openDisplay,
      id,
      changeScale,
    } = this.props;

    const portalStyle = {
      position: 'absolute',
      top: `${this.state.top - 1}px`,
      left: `${this.state.left}px`,
      right: 'auto',
      bottom: 'auto',
      borderRadius: '0',
    };

    const storageMenuPortal = ({
      openPortal,
      closePortal,
      isOpen,
      portal,
    }) => [
      <a
        key="storagebtn"
        className={`toolbar__button${isOpen ? ' toolbar__item--open' : ''}`}
        onClick={this.openStorageMenu(openPortal)}
      >
        Storage
      </a>,
      portal(storageMenu({
        id,
        closePortal,
        portalStyle,
        selectItem: this.selectStorageMenuItem,
      })),
    ];

    return (
      <header className="toolbar">
        <ul className="toolbar__items">
          <li className="toolbar__item">
            <img className="toolbar__logo" src="static/vf.icon.svg" alt="Fiha logo" />
          </li>
          <li className="toolbar__item">
            <PortalWithState closeOnOutsideClick closeOnEsc>
              {storageMenuPortal}
            </PortalWithState>
          </li>

          <li className="toolbar__item">
            <a className="toolbar__button" onClick={toggleAnimation}>{animate ? 'Pause' : 'Play'}</a>
          </li>

          <li className="toolbar__item">
            <a className="toolbar__button" onClick={openDisplay}>Open display</a>
          </li>

          <li className="toolbar__item toolbar__item--btn-group">
            <div className="btn-group btn-group-sm">
              <button
                className="btn btn-default"
                disabled={scale === 0.5}
                onClick={() => changeScale(0.5)}
              >
                0.5
              </button>
              <button
                className="btn btn-default"
                disabled={scale === 0.625}
                onClick={() => changeScale(0.625)}
              >
                0.625
              </button>
              <button
                className="btn btn-default"
                disabled={scale === 0.75}
                onClick={() => changeScale(0.75)}
              >
                0.75
              </button>
              <button
                className="btn btn-default"
                disabled={scale === 0.875}
                onClick={() => changeScale(0.875)}
              >
                0.875
              </button>
              <button
                className="btn btn-default"
                disabled={scale === 1}
                onClick={() => changeScale(1)}
              >
                1
              </button>
            </div>
          </li>

          <li className="toolbar__item toolbar__item--divider" />

          <li className="toolbar__item toolbar__item--spacer" />
          <li className="toolbar__item">
            <span className="toolbar__label">{id}</span>
          </li>
        </ul>
      </header>
    );
  }
}

Toolbar.propTypes = {
  openStorageMenu: PropTypes.func.isRequired,
  animate: PropTypes.bool,
  toggleAnimation: PropTypes.func.isRequired,
  openDisplay: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  scale: PropTypes.number.isRequired,
  changeScale: PropTypes.func.isRequired,
};

Toolbar.defaultProps = {
  animate: false,
};
