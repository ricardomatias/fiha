import React from 'react';
import ReactModal from 'react-modal';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { ToastContainer, toast } from 'react-toastify';
import PropTypes from 'prop-types';
import AudioService from '../services/audio';
import AudioManager from './audio-manager';
import ControlLayers from './control-layers';
import FihaEditor from './fiha-editor';
import MIDIDevices from './control-midi';
import Toolbar from './toolbar';
import StorageDialog from './storage-forage';
import GistDialog from './storage-gist';
import { getGistId } from '../services/gist';
import storage from '../fiha-storage';
import fihaMIDI, { FihaMIDI } from '../services/midi/midi';
import KeyboardService from '../services/keyboard';
import MouseService from '../services/mouse';

import './storage-dialog.scss';

import { validateCode } from '../../display/function-compiler';
import {
  argumentNames as canvasArgumentNames,
  apiReference as canvasReference,
} from '../../display/components/canvas-display-layer';
import {
  argumentNames as threeArgumentNames,
  apiReference as threeReference,
} from '../../display/components/three-display-layer';
import {
  argumentNames as paperArgumentNames,
  apiReference as paperReference,
} from '../../display/components/paper-display-layer';

import './fiha.scss';

const argumentNames = {
  canvas: canvasArgumentNames,
  three: threeArgumentNames,
  paper: paperArgumentNames,
};

const apiReferences = {
  canvas: canvasReference,
  three: threeReference,
  paper: paperReference,
};

const {
  BroadcastChannel,
  addEventListener,
  removeEventListener,
} = window;

const defaultShortcuts = [
  {
    ctrl: true,
    key: 's',
    name: 'saveSetup',
  },
  {
    shift: true,
    ctrl: true,
    key: 's',
    name: 'openStorageMenu',
    args: ['local', 'save'],
  },
  {
    shift: true,
    ctrl: true,
    key: 'g',
    name: 'openStorageMenu',
    args: ['gist', 'save'],
  },
  {
    shift: true,
    ctrl: true,
    key: 'e',
    name: 'exportSetup',
  },
  {
    ctrl: true,
    key: 'r',
    name: 'loadSetup',
  },
  {
    ctrl: true,
    key: 'o',
    name: 'openStorageMenu',
    args: ['local', 'load'],
  },
  {
    ctrl: true,
    key: 'p',
    name: 'toggleAnimation',
  },
  {
    ctrl: true,
    key: 'd',
    name: 'openDisplay',
  },
];


export default class Fiha extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scale: 1,
      bpm: 120,
    };

    this.downloadLink = document.createElement('a');

    this.initializeController();
  }

  componentDidMount() {
    addEventListener('keydown', this.shortcutListener);

    const opts = { passive: true, capture: false };
    if (!this.broadcastChannel) {
      this.broadcastChannel = new BroadcastChannel(this.props.broadcastChannelId);

      this.broadcast = (action) => {
        if (this.emit) {
          this.emit({
            type: 'BROADCAST',
            payload: action,
          });
        }
      };
    }

    this.props.worker.removeEventListener('message', this.workerListener, opts);
    this.props.worker.addEventListener('message', this.workerListener, opts);

    this.emit({
      type: 'SET_BROADCAST_ID',
      payload: {
        id: this.props.broadcastChannelId,
      },
    });

    this.keyboard = new KeyboardService({
      dispatch: this.emit,
    });

    if (this.props.animate) this.animate();

    const gistId = getGistId();
    if (gistId) {
      this.loadGist(gistId, (loaded) => {
        this.emit({
          type: 'APPLY_SETUP',
          payload: loaded,
        });
      });
    }
  }

  componentWillUnmount() {
    if (this.aFRID) cancelAnimationFrame(this.aFRID);
    removeEventListener('keydown', this.shortcutListener);

    [
      ['broadcastChannel', 'close'],
      ['audio', 'close'],
      ['midi', 'close'],
      ['mouse', 'destroy'],
      ['keyboard', 'destroy'],
    ].forEach((info) => {
      const [name, method] = info;
      if (this[name]) this[name][method]();
      this[name] = null;
    });
  }

  get edited() {
    const { edit, layers } = this.props;
    return layers.find(layer => layer.id === edit.id);
  }

  get modalContent() {
    const {
      loadSetup,
      loadCode,
      importCode,
      saveSetup,
      closeStorageDialog,
      loadGist,
      saveGist,
      state,
      props,
    } = this;
    const { storageModalOpen, storageAction } = state;
    const { replaceSetup, id } = props;

    if (storageModalOpen === 'local') {
      return (
        <StorageDialog
          storage={storage}
          mode={storageAction || 'load'}
          load={loadSetup}
          loadCode={loadCode}
          apply={replaceSetup}
          importCode={importCode}
          save={saveSetup}
          id={id}
          close={closeStorageDialog}
        />
      );
    }

    if (storageModalOpen === 'gist') {
      return (
        <GistDialog
          save={saveGist}
          load={loadGist}
          loadCode={loadCode}
          apply={replaceSetup}
          importCode={importCode}
          id={id}
          close={closeStorageDialog}
        />
      );
    }

    return null;
  }

  initializeController() {
    `emit
    refDisplay
    refEditor
    changeScale
    openStorageMenu
    openDisplay
    closeStorageDialog
    workerListener
    shortcutListener
    animate
    onBpmChange
    handleEditLayer
    handleAddLayer
    handleRemoveLayer
    handleSortLayers
    loadSetup
    loadGist
    saveGist
    loadCode
    importCode
    saveSetup
    exportSetup`.split(/[\s]+/m)
      .forEach((key) => {
        this[key] = this[key].bind(this);
      });

    this.data = {};
    this.workerCallbacks = {};

    this.midi = fihaMIDI({
      dispatch: this.emit,
    });

    this.audio = new AudioService();
  }

  // eslint-disable-next-line react/sort-comp
  onBpmChange(bpm) {
    this.setState({ bpm });
    this.emit({
      type: 'SET_BPM',
      payload: {
        bpm,
      },
    });
  }

  emit(data, callback = null) {
    const sent = { ...data };
    if (callback) {
      const id = `cb${(performance.now() * 10000).toFixed()}`;
      sent.payload = data.payload || {};
      sent.payload.f_callbackId = id;
      this.workerCallbacks[id] = callback;
    }
    this.props.worker.postMessage(sent);
  }

  animate() {
    if (this.props.animate) {
      this.emit({
        type: 'UPDATE_DATA',
        payload: this.audio.data,
      });
    }
    this.aFRID = setTimeout(this.animate, 0);
  }

  shortcutListener(evt) {
    const shortcut = this.props.shortcuts.find(sc =>
      sc.key.toLowerCase() === evt.key.toLowerCase() &&
      !sc.ctrl === !evt.ctrlKey &&
      !sc.alt === !evt.altKey &&
      !sc.shift === !evt.shiftKey);

    if (!shortcut) return;

    const func = (this[shortcut.name] || this.props[shortcut.name]);
    if (!func) return;
    evt.preventDefault();

    func(...(shortcut.args || []));
  }

  workerListener(evt) {
    const { data } = evt;
    const { payload } = data;
    switch (data.type) {
      case 'WORKER_CALLBACK':
        break;
      case 'REPLACE_SETUP':
        this.props.replaceSetup(payload);
        break;
      case 'NOTIFICATION':
        toast[payload.type || 'warning'](payload.message);
        break;
      case 'EXECUTION_ERROR':
        if (this.editor) {
          this.editor.setExecutionError(payload);
        }
        break;
      default:
        toast.warning(`Unknown "${data.type}" action`);
    }

    if (payload && payload.f_callbackId &&
        typeof this.workerCallbacks[payload.f_callbackId] === 'function') {
      const id = payload.f_callbackId;
      delete payload.f_callbackId;
      this.workerCallbacks[id](payload);
      delete this.workerCallbacks[id];
    }
  }

  openDisplay() {
    window.open(
      `/display/?bcid=${this.props.broadcastChannelId}`,
      'fiha-display',
      'menubar=no,toolbar=no,location=no',
    );
  }

  openStorageMenu(storageName, actionName = 'save') {
    this.setState({
      storageAction: actionName,
      storageModalOpen: storageName,
    });
  }

  closeStorageDialog() {
    this.setState({ storageModalOpen: false });
  }

  saveSetup(id = this.props.id) {
    this.emit({
      type: 'STORAGE_SAVE',
      payload: {
        layers: this.props.layers,
        id,
      },
    });
    this.closeStorageDialog();
  }

  loadSetup(id = this.props.id) {
    this.emit({
      type: 'STORAGE_LOAD',
      payload: { id },
    });
    this.closeStorageDialog();
  }

  loadCode(id = this.props.id) {
    return (cb) => {
      storage.getItem(id)
        .then((found) => {
          if (!found) {
            cb(new Error(`Could not find setup "${id}"`));
            return;
          }

          cb(null, JSON.stringify(found, null, 2));
        })
        .catch(cb);
    };
  }

  importCode(id, code) {
    const { loadSetup } = this;
    return (cb) => {
      storage.setItem(id, JSON.parse(code))
        .then(() => {
          loadSetup();
          cb();
        })
        .catch(cb);
    };
  }

  exportSetup() {
    const { downloadLink } = this;
    const content = `data:text/plain,export default ${JSON.stringify({
      id: this.props.id,
      layers: this.props.layers,
    }, null, 2)}

    `;
    downloadLink.setAttribute('download', `${this.props.id}.js`);
    downloadLink.setAttribute('href', content);
    downloadLink.click();
  }

  loadGist(id, cb = () => {}) {
    this.emit({
      type: 'GIST_LOAD',
      payload: { id },
    }, cb);
  }

  saveGist(id, description, cb = () => {}) {
    this.emit({
      type: 'GIST_SAVE',
      payload: {
        id,
        description,
        layers: this.props.layers,
      },
    }, cb);
  }

  handleEditLayer(payload) {
    this.broadcast({
      type: 'EDIT_LAYER',
      payload,
    });
    this.props.handleEditLayer(payload);
  }

  handleAddLayer(type, id) {
    const payload = {
      id,
      type,
      setup: '/*..*/',
      animation: '/*..*/',
    };

    this.broadcast({
      type: 'ADD_LAYER',
      payload,
    });
    this.props.handleAddLayer(type, id);
  }

  handleRemoveLayer(id) {
    this.broadcast({
      type: 'REMOVE_LAYER',
      payload: { id },
    });
    this.props.handleRemoveLayer(id);
  }

  handleSortLayers(oldIndex, newIndex) {
    this.broadcast({
      type: 'SORT_LAYERS',
      payload: { oldIndex, newIndex },
    });
    this.props.handleSortLayers(oldIndex, newIndex);
  }

  refDisplay(el) {
    this.displayComponent = el;
    if (el) {
      this.mouse = new MouseService({
        element: el,
        dispatch: this.emit,
      });
    } else {
      this.mouse.destroy();
    }
  }

  changeScale(value) {
    this.setState({ scale: value });
    this.broadcast({
      type: 'DISPLAY_SCALE',
      payload: {
        scale: value,
      },
    });
  }

  refEditor(editor) {
    this.editor = editor;
  }

  render() {
    const { storageModalOpen, scale } = this.state;
    const { edit, id } = this.props;
    const editedLayer = this.edited;
    const editedScript = editedLayer ? editedLayer[edit.role] : `/* no ${edit.role} script of type ${edit.type} found */`;

    const toolbarProps = {
      id: id || 'gist',
      scale,
      animate: this.props.animate,
      openStorageMenu: this.openStorageMenu,
      toggleAnimation: this.props.toggleAnimation,
      openDisplay: this.openDisplay,
      changeScale: this.changeScale,
    };

    const modalProps = {
      onRequestClose: this.closeStorageDialog,
      isOpen: !!storageModalOpen, //eslint-disable-line
      shouldCloseOnOverlayClick: true,
      contentLabel: 'Storage Dialog',
      portalClassName: 'storage-dialog',
      closeTimeoutMS: 218,
      className: {
        base: 'storage-dialog__content',
        afterOpen: 'storage-dialog__content--after-open',
        beforeClose: 'storage-dialog__content--before-close',
      },
      overlayClassName: {
        base: 'storage-dialog__overlay',
        afterOpen: 'storage-dialog__overlay--after-open',
        beforeClose: 'storage-dialog_u_overlay--before-close',
      },
    };

    const editorProps = {
      ...edit,
      ref: this.refEditor,
      value: editedScript || '',
      handleEditorChange: this.handleEditLayer,
      validateCode: code => validateCode(code, ...argumentNames[edit.type]),
      codeGlobals: argumentNames[edit.type],
      apiReference: apiReferences[edit.type],
    };

    const iframeUrl = `./display/?bcid=${this.props.broadcastChannelId}&did=controller`;

    return [
      <Toolbar key="toolbar" {...toolbarProps} />,

      <ToastContainer key="toast-container" />,

      <ReactModal key="modal" {...modalProps} ariaHideApp={false}>
        {this.modalContent}
      </ReactModal>,

      <div key="layout" className="fiha-info__holder fiha-layout d-flex align-items-stretch">
        <div key="col-1" className="display-n-controls">
          <div ref={this.refDisplay} className="display-layers">
            <iframe title="display" src={iframeUrl} />
          </div>

          <Tabs className="controls">
            <TabList>
              <Tab>Layers</Tab>
              {FihaMIDI.capable ? (<Tab>MIDI</Tab>) : ''}
              <Tab>Audio</Tab>
            </TabList>

            <TabPanel>
              <ControlLayers
                key="layers"
                {...this.props}
                handleAddLayer={this.handleAddLayer}
                handleRemoveLayer={this.handleRemoveLayer}
                handleEditLayer={this.handleEditLayer}
                handleSortLayers={this.handleSortLayers}
              />
            </TabPanel>
            {FihaMIDI.capable ? (
              <TabPanel>
                <MIDIDevices key="midi" midi={this.midi} />
              </TabPanel>
            ) : ''}
            <TabPanel>
              <AudioManager
                onBpmChange={this.onBpmChange}
                bpm={this.state.bpm}
                key="audio"
                audio={this.audio}
              />
            </TabPanel>
          </Tabs>
        </div>

        <FihaEditor
          autoApply
          {...editorProps}
        />
      </div>,
    ];
  }
}

Fiha.propTypes = {
  animate: PropTypes.bool.isRequired,
  broadcastChannelId: PropTypes.string.isRequired,
  shortcuts: PropTypes.array,
  handleEditLayer: PropTypes.func,
  handleSortLayers: PropTypes.func,
  handleAddLayer: PropTypes.func,
  handleRemoveLayer: PropTypes.func,
  toggleAnimation: PropTypes.func,
  replaceSetup: PropTypes.func,
  worker: PropTypes.instanceOf(Worker).isRequired,
  id: PropTypes.string.isRequired,
  layers: PropTypes.array.isRequired,
  edit: PropTypes.shape({
    value: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
    role: PropTypes.string,
  }),
};

// eslint-disable-next-line no-console
const missigAction = name => () => console.log('missing %s', name);

Fiha.defaultProps = {
  shortcuts: defaultShortcuts,
  handleEditLayer: missigAction('handleEditLayer'),
  handleSortLayers: missigAction('handleSortLayers'),
  handleAddLayer: missigAction('handleAddLayer'),
  handleRemoveLayer: missigAction('handleRemoveLayer'),
  toggleAnimation: missigAction('toggleAnimation'),
  replaceSetup: missigAction('replaceSetup'),
  edit: {
    value: '',
    id: '',
    type: '',
    role: '',
  },
};
