import React from 'react';
import PropTypes from 'prop-types';
import Highlight from 'react-highlight';
import 'highlight.js/styles/monokai-sublime.css'; // eslint-disable-line import/no-extraneous-dependencies
import { ToastContainer, toast } from 'react-toastify';
import fihaMIDI from '../../controller/services/midi/midi';
import AudioService from '../../controller/services/audio';
import MouseService from '../../controller/services/mouse';
import KeyboardService from '../../controller/services/keyboard';
import initWorker from '../embedded.worker';
import DisplayLayers from '../../display/components/display-layers';
import './embedded-display.scss';

export const LayerInfo = ({
  id,
  type,
  animation,
  setup,
  active,
}) => (
  <div className={`row layer-info ${active && 'layer-info--active'}`}>
    <div className="col">
      <div className="row py-3 layer-info__meta">
        <div className="col">
          <span className="info-layer__id h3">
            {id}
          </span>{' '}
          <span className="badge badge-pill badge-info info-layer__type">
            {type}
          </span>
        </div>
      </div>

      <div className="row layer-info__scripts">
        <div className="col-lg-6 col-12 layer-info__script layer-info__script--setup">
          <strong>Setup script:</strong>
          <Highlight
            className="javascript"
          >
            {setup}
          </Highlight>
        </div>
        <div className="col-lg-6 col-12 layer-info__script layer-info__script--animation">
          <strong>Animation script:</strong>
          <Highlight
            className="javascript"
          >
            {animation}
          </Highlight>
        </div>
      </div>
    </div>
  </div>
);

LayerInfo.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  setup: PropTypes.string.isRequired,
  animation: PropTypes.string.isRequired,
  active: PropTypes.bool,
};

LayerInfo.defaultProps = {
  active: false,
};


export const EmbeddedHeader = ({
  id,
  gistId,
  description,
  author,
  authorAvatar,
  layers,
  showCode,
  toggleCode,
}) => (
  <header className="embedded__header">
    <div className="embedded__header-top">
      <div className="embedded__header-top-left">
        <a
          href="../"
          title="Fiha"
          className="embedded__logo-link"
        >
          <svg
            version="1.1"
            viewBox="0 0 1000 1000"
            height="1000"
            width="1000"
            className="embedded__logo"
          >
            <path
              style={{
                stroke: 'none',
                strokeWidth: 0,
                fill: '#f8f8f2',
                strokeLinecap: 'round',
                strokeLinejoin: 'round',
              }}
              id="fiha-line"
              d="m 42.144772,100.70423 460.738378,798.02231 137.92084,-238.11891 -100.83949,0 -83.42113,-144.48961 267.68175,0 78.38889,-135.77355 -424.45952,0 -83.42215,-144.49139 591.30382,0 78.02825,-135.14885 z"
            />
          </svg>
        </a>
        <h1 className="embedded__title">{id}</h1>
      </div>

      <div className="embedded__header-top-right">
        {(
          author && <h2 className="embedded__author"> by {author}</h2>
        )}
        {(
          authorAvatar && <img src={authorAvatar} alt="author avatar" className="embedded__author-avatar" />
        )}
      </div>
    </div>

    <div className="embedded__header-bottom container-fluid">
      <div className="row py-1 align-items-baseline">
        <div className="col embedded__description">
          {description}
        </div>

        <div className="col btn-group embedded__actions">
          <button
            className="btn btn-sm btn-secondary"
            onClick={toggleCode}
          >
            {showCode ? 'Hide' : 'Show'} code
          </button>
          <a
            className="btn btn-sm btn-primary"
            href={`./../controller/#gid=${gistId}`}
            target="fiha-controller"
          >
            Edit
          </a>
        </div>
      </div>
      {(
        showCode && (
          <div className="row layers-info">
            <div className="col">
              {layers.map(layer => (<LayerInfo {...layer} key={layer.id} />))}
            </div>
          </div>
        )
      )}
    </div>
  </header>
);

EmbeddedHeader.propTypes = {
  id: PropTypes.string,
  description: PropTypes.string,
  author: PropTypes.string,
  authorAvatar: PropTypes.string,
  gistId: PropTypes.string.isRequired,
  layers: PropTypes.array,
  showCode: PropTypes.bool,
  toggleCode: PropTypes.func.isRequired,
};

EmbeddedHeader.defaultProps = {
  id: null,
  description: null,
  author: null,
  authorAvatar: null,
  layers: [],
  showCode: false,
};


/* eslint-disable-next-line */
export default class EmbeddedDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      author: null,
      authorAvatar: null,
      description: null,
      created: null,
      updated: null,
      comments: null,
      history: null,
      forks: null,
      showCode: false,
    };
    this.refDisplay = this.refDisplay.bind(this);

    this.worker = initWorker();
    this.workerListener = this.workerListener.bind(this);
    this.emit = this.emit.bind(this);
    this.workerCallbacks = {};

    this.aFRID = false;
    this.animate = this.animate.bind(this);
    this.toggleCode = this.toggleCode.bind(this);
  }

  // eslint-disable-next-line
  workerListener(evt) {
    const { data } = evt;

    switch (data.type) {
      case 'WORKER_CALLBACK':
        break;
      case 'NOTIFICATION':
        toast[data.payload.type || 'warning'](data.payload.message);
        break;
      default:
        toast.warning(`Unknown "${data.type}" action`);
    }

    if (data.payload &&
        data.payload.f_callbackId &&
        typeof this.workerCallbacks[data.payload.f_callbackId] === 'function') {
      this.workerCallbacks[data.payload.f_callbackId](data.payload);
      delete this.workerCallbacks[data.payload.f_callbackId];
    }
  }

  emit(data, callback = null) {
    const sent = { ...data };
    if (callback) {
      const id = `cb${(performance.now() * 10000).toFixed()}`;
      sent.payload = data.payload || {};
      sent.payload.f_callbackId = id;
      this.workerCallbacks[id] = callback;
    }
    this.worker.postMessage(sent);
  }

  componentDidMount() {
    this.worker.addEventListener('message', this.workerListener, {
      capture: false,
      passive: true,
    });

    const noCbEmit = data => this.emit(data);

    this.keyboard = new KeyboardService({
      dispatch: noCbEmit,
    });

    this.midi = fihaMIDI({
      dispatch: noCbEmit,
    });

    this.audio = new AudioService();

    this.emit({
      type: 'SET_BROADCAST_ID',
      payload: {
        id: this.props.broadcastChannelId,
      },
    }, () => {
      this.emit({
        type: 'LOAD_GIST',
        payload: {
          id: this.props.gistId,
        },
      }, json => this.setState(json));
    });

    this.animate();
  }

  componentWillUnmount() {
    this.worker.removeEventListener('message', this.workerListener, {
      capture: false,
      passive: true,
    });

    [
      // ['broadcastChannel', 'close'],
      ['audio', 'close'],
      ['midi', 'close'],
      ['mouse', 'destroy'],
      ['keyboard', 'destroy'],
    ].forEach((info) => {
      const [name, method] = info;
      if (this[name]) this[name][method]();
      this[name] = null;
    });
  }

  toggleCode() {
    this.setState({ showCode: !this.state.showCode });
  }

  animate() {
    if (this.audio) {
      this.emit({
        type: 'UPDATE_DATA',
        payload: this.audio.data,
      });
    }
    this.aFRID = setTimeout(this.animate, 0);
  }

  refDisplay(el) {
    this.displayComponent = el;
    if (el) {
      this.mouse = new MouseService({
        element: el.dom,
        dispatch: this.emit,
      });
    } else if (this.mouse) {
      this.mouse.destroy();
    }
  }

  render() {
    const props = {
      ...this.props,
      id: this.props.gistId,
      mode: 'standalone',
    };
    const presProps = {
      ...this.state,
      gistId: props.gistId,
      toggleCode: this.toggleCode,
    };

    return [
      <EmbeddedHeader {...presProps} key="header" />,
      <ToastContainer key="toast-container" />,
      <DisplayLayers {...props} ref={this.refDisplay} key="display" />,
    ];
  }
}

EmbeddedDisplay.propTypes = {
  gistId: PropTypes.string.isRequired,
  broadcastChannelId: PropTypes.string.isRequired,
};
