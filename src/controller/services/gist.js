import querystring from 'querystring';

export const getGistId = () => {
  const qs = querystring.parse(window.location.hash.slice(1));
  return qs.gid;
};

export const json2setup = (json) => {
  if (json.errors && json.errors.length) throw new Error(`Gist API error: ${json.errors[0].code}`);

  const layers = [];
  const meta = JSON.parse(json.files['fiha.json'].content);

  Object.keys(meta.layers).forEach((id) => {
    const info = meta.layers[id];

    layers.push({
      id,
      type: info.type,
      setup: (json.files[info.setup] || {}).content || '',
      animation: (json.files[info.animation] || {}).content || '',
    });
  });

  return {
    layers,
    id: json.id,
    author: json.owner.login,
    authorAvatar: json.owner.avatar_url,
    description: json.description,
    created: json.created_at,
    updated: json.updated_at,
    comments: json.comments,
    forks: json.forks,
    history: json.history.map(item => ({
      author: item.user.login,
      authorAvatar: item.user.avatar_url,
      updated: item.committed_at,
      version: item.version,
    })),
  };
};

export const userQuery = `{
  viewer {
    login
    avatarUrl
    name
  }
}`;

export const listQuery = `{
  viewer {
    login
    gists(orderBy: {field: CREATED_AT, direction: DESC}, first: 10) {
      nodes {
        description
        updatedAt
        createdAt
        pushedAt
        isPublic
        owner {
          login
          avatarUrl
        }
        comments(last: 10) {
          totalCount
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
}
`;

export const pluck = (arr, prop) => arr.map(item => item[prop]);

export const removed = (before, after) => before.filter(item => after.indexOf(item) < 0);

export default class GistService {
  constructor(storage) {
    this.storage = storage;
  }

  get user() {
    return this.storage.getItem('gist');
  }

  get reqAuth() {
    return this.user
      .then(user => ({
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          Authorization: `token ${user.token}`,
          'content-type': 'application/json',
        },
      }));
  }

  async credentials(info = {}) {
    if (info && info.token) {
      const response = await fetch('https://api.github.com/graphql', {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          Authorization: `bearer ${info.token}`,
          'content-type': 'application/json',
        },
        body: JSON.stringify({ query: userQuery }),
      });

      const json = await response.json();
      const saved = {
        ...info,
        ...json.data.viewer,
      };

      return this.storage.setItem('gist', saved)
        .then(() => saved);
    }

    return this.storage.removeItem('gist');
  }

  list() {
    return this.user
      .then(user => fetch('https://api.github.com/graphql', {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          Authorization: `bearer ${user.token}`,
        },
        body: JSON.stringify({ query: listQuery }),
      })
        .then(response => response.json()));
  }

  async fork(originalId, info) {
    return this.reqAuth
      .then(options => fetch(`https://api.github.com/gists/${originalId}/forks`, options)
        .then(response => response.json())
        .then(json => this.save({
          desciption: json.description,
          ...info,
          id: json.id,
        })));
  }

  processSave(info, remove = []) {
    const files = {};
    const meta = {
      api: 2,
      layers: {},
    };

    info.layers.forEach((layer) => {
      const id = layer.id.trim().replace(/[^a-z0-9]/ig, '-');
      files[`layer__${id}__setup.js`] = { content: layer.setup || '/*  */' };
      files[`layer__${id}__animation.js`] = { content: layer.animation || '/*  */' };
      meta.layers[layer.id] = {
        active: layer.active,
        type: layer.type,
        setup: `layer__${layer.id}__setup.js`,
        animation: `layer__${layer.id}__animation.js`,
      };
    });

    // instructs renoval
    remove.forEach((id) => {
      files[`layer__${id}__setup.js`] = null;
      files[`layer__${id}__animation.js`] = null;
    });

    const url = `https://api.github.com/gists${info.id ? `/${info.id}` : ''}`;
    return this.reqAuth
      .then(options => fetch(url, {
        ...options,
        method: info.id ? 'PATCH' : 'POST',
        body: JSON.stringify({
          public: info.public || false,
          description: info.description || 'Visual Fiha Setup',
          files: Object.assign({
            'fiha.json': { content: JSON.stringify(meta, null, 2) },
          }, files),
        }),
      })
        .then(response => response.json())
        .then(json2setup));
  }

  async save(info) {
    if (info.id) {
      const user = await this.user;
      const loaded = await this.load(info.id);

      // TODO: compare the loaded layers files with info.layers
      // to properly remove the file from the gist when obsolete
      if (loaded.author !== user.login) {
        return this.fork(info.id, info);
      }

      // update gist
      const remove = removed(pluck(loaded.layers, 'id'), pluck(info.layers, 'id'));
      return this.processSave(info, remove);
    }

    // save as new gist
    return this.processSave(info);
  }

  load(id) {
    return this.reqAuth
      .then(options => fetch(`https://api.github.com/gists/${id}`, {
        headers: options.headers,
      })
        .then(response => response.json())
        .then(json2setup));
  }
}
