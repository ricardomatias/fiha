import React from 'react';
import { Provider } from 'react-redux';
import initWorker from './fiha.worker';
import { connector } from './configure-store';
import store from './store';
import Fiha from './components/fiha';

const ConnectedFiha = connector(Fiha);

export default class FihaRedux extends React.Component {
  constructor(props) {
    super(props);
    this.worker = initWorker();
  }

  render() {
    return (
      <Provider store={store}>
        <ConnectedFiha worker={this.worker} />
      </Provider>
    );
  }
}
